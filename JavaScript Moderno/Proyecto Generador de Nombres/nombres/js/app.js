
document.querySelector('#generar-nombre').addEventListener('submit', cargarNombre );

//Llamado AJAX e imprimir resultados
function cargarNombre( e ) {
    e.preventDefault();

    //Leer las variables
    const origen    = document.getElementById('origen');
    const origenSelect = origen.options[origen.selectedIndex].value;

    const genero    = document.getElementById('genero');
    const generoSelect = genero.options[genero.selectedIndex].value;

    const cantNombres   = document.getElementById('numero').value;

    let url = '';
    url += 'http://uinames.com/api/?';
// Si hay origen agregarlo a la URL
    if ( origenSelect !== '' ) {
        url += `region=${origenSelect}&`;
    }
    //Si hay un genero agregarlo a la URL
    if ( generoSelect !== '' ) {
        url += `gender=${generoSelect}&`;
    }

    if ( cantNombres !== '' ) {
        url += `amount=${cantNombres}`;
    }
    console.log(origenSelect);
    console.log(generoSelect);
    console.log(cantNombres);
    const xhr = new XMLHttpRequest();

    xhr.open('GET', url, true);

    xhr.onload = function() {
        if ( this.status === 200 ) {
            console.log(JSON.parse(this.responseText));
            const nombres = JSON.parse( this.responseText );

            let htmlNombres = `<div class="card">
                                    <div class="card-header">
                                        Nombres Generados
                                    </div>
                                    <ul class="list-group list-group-flush">
                                `;
            nombres.forEach(nombre => {
                htmlNombres += `
                    <li class="list-group-item">${nombre.name}</li>
                `;
            });
            htmlNombres += '</ul></div>'
            document.getElementById('resultado').innerHTML = htmlNombres;
        }  
    };
    
    xhr.send();
}