// Callback en Foeach
const ciudades = ['Nagarote', 'Leon', 'Managua','Chinandega', 'Carazo'];

//Inline Callback
//funcion anonima
ciudades.forEach(function(ciudad) {
    console.log(ciudad);
})

// Con funcion definida
//no usar
function imprimirCiudad( ciudad ) {
    console.warn(ciudad);
}

ciudades.forEach(imprimirCiudad)

// Listado de paises
const paises = ['Nicaragua', 'Francia','Alemania', 'EEUU','Rusia', 'Canada'];
// Se agrega un nuevo pais despues de 2 segundos
function nuevoPais(pais, callback) {
    setTimeout(() => {
        paises.push(pais);
        callback();
    }, 2000);
}


// Los paises se muestran despues de un segundo
function mostrarPaises(){
    setTimeout(() => {
        let html = '<ul>';
        paises.forEach(function(pais) {
            html += `<li>${pais} </li>`
        })
        html += '</ul>'
        document.querySelector('#app').innerHTML = html;
    }, 1000);
}

//Agregar nuevo pais
nuevoPais('Holanda', mostrarPaises);

// Mostrar los paises
mostrarPaises();