// Promises

const esperando = new Promise( function(resolve, reject) {
    setTimeout(() => {
        resolve('Se Ejecuto');
    }, 5000);
} );

esperando
.then((resultado) => {
    console.log(resultado);
    
})
.catch((err) => {
    console.error(err);
})

// Ejemplo Catch
const aplicarDescuento = new Promise((resolve, reject) => {
    const descuento = false;
    if ( descuento ) {
        resolve('Descuento Aplicado');
    } else {
        reject('No se puede aplicar descuento');
    }
});

aplicarDescuento
.then((resultado) => {
    console.log(resultado);
    
})
.catch((error) => {
    console.error(error);
    
})