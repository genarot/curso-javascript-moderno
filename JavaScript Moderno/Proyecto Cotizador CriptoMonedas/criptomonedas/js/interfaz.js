class Interfaz {

    constructor () {
        this.init();
    }
    
    init() {
        this.construirSelect();
    }
    construirSelect()  {
        cotizador.obtenerMonedasAPI()
        .then(monedas => {
            //Crear un select con las opciones
            let arregloMonedas = monedas.monedas;
            let select          = document.getElementById('criptomoneda');
            console.log(monedas);
            for ( let moneda in arregloMonedas) {
                // console.log(arregloMonedas[moneda].symbol);
                const option    = document.createElement('option');
                option.value    = arregloMonedas[moneda].id;
                option.setAttribute('data-symbol', arregloMonedas[moneda].symbol);
                option.appendChild( document.createTextNode(arregloMonedas[moneda].name));
                select.appendChild(option);
            }
          
        })
        .catch(err => {
            console.error(err);
            
        })
    }

    mostrarMensaje( mensaje, clases ) {
        const div = document.createElement('div');
        div.className   = clases;
        div.appendChild(document.createTextNode(mensaje))
        document.querySelector('.mensajes').appendChild(div);
        setTimeout(() => {
            div.remove();
        },2000)
    }

    //Imprime el resultado de la cotizacion
    mostrarResultado( resultado, moneda ) {
        //Eliminamos el resultado anterior en caso de existir
        const resultadoAnterior = document.querySelector('#resultado > div');
        if ( resultadoAnterior) {
            resultadoAnterior.remove();
        }

        // Muestra el Spinner
        this.mostrarSpinner();

        const div = document.createElement('div');
        //Leer valor de la moneda
        const valor =  resultado.quotes[moneda]; 

        let horaActualizacion = new Date(resultado.last_updated);
        //Construir el template
        let templateHTML = '';
        templateHTML += `
            <div class="card cyan darken-3">
                <div class="card-content white-text">
                    <span class="card-title">Resultado:</span>
                    <p>El precio de ${resultado.name} a moneda ${moneda} es de: ${valor.price}</p>
                    <p>Ultima hora: ${valor.percent_change_1h} </p>
                    <p>Ultimo dia: ${valor.percent_change_24h} </p>
                    <p>Ultimos 7 dias: ${valor.percent_change_7d} </p>
                    <p>Ultima Actualizacion: ${horaActualizacion.getHours()}:${horaActualizacion.getMinutes()}:${horaActualizacion.getSeconds()}</p>
                </div>
            </div>
        `;

        //Oculta el spinner y muestra el resultado
        setTimeout(() => {
            //Imprimir el resultado
            document.getElementById('resultado').innerHTML = templateHTML;

            //Ocultar el spinner
            document.querySelector('.spinner img').remove();
        },3000)
    }

    //Muestra un spinner cuando se cotiza
    mostrarSpinner() {
        const spinnerGIF = document.createElement('img');
        spinnerGIF.src = 'img/spinner.gif';
        document.querySelector('.spinner').appendChild(spinnerGIF);
    }
}