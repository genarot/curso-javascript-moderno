class Cotizador {

    //Obtiene todo el JSON con las cryptomoneda
    async obtenerMonedasAPI() {
        // Fetch a la API
        const urlObtenenerMonedas =  await fetch('https://api.coinmarketcap.com/v2/ticker/');

        //Respuesta en JSON a las monedas
        const monedas =   await urlObtenenerMonedas.json();

        return { monedas: monedas.data }
    }

    async obtenerValores( moneda, criptomoneda) {
        const urlConvertir = await fetch(`https://api.coinmarketcap.com/v2/ticker/${criptomoneda}/?convert=${moneda}`);

        //Respuesta en JSON
        const resultado = await urlConvertir.json();

        return { resultado : resultado.data };
    }
}