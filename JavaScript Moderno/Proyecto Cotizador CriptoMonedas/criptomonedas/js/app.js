const cotizador = new Cotizador();
const ui    = new Interfaz();

//Obtener el formulario
const formulario =  document.getElementById('formulario');
//Event Listener cuando se envia el formulario
document.addEventListener('submit', (evt) => {
    evt.preventDefault();

    //Leer la moneda seleccionada
    const   selectMoneda = document.getElementById('moneda');
    const   monedaSeleccionada = selectMoneda.options[selectMoneda.selectedIndex].value;
    
    if ( monedaSeleccionada === '' ) {
        ui.mostrarMensaje('Selecciona una moneda.', 'deep-orange darken-4 card-panel')
    }

    // Leer la criptomoneda seleccionada
    const selectCriptomoneda = document.getElementById('criptomoneda');
    const criptomonedaSeleccionada = selectCriptomoneda.options[selectCriptomoneda.selectedIndex].value;

    if ( criptomonedaSeleccionada === '' ) {
        ui.mostrarMensaje('Selecciona una criptomoneda', 'deep-orange darken-4 card-panel');
    }

    if ( monedaSeleccionada === '' || criptomonedaSeleccionada === '' ) {
        console.warn('Faltan datos');
        return;
    } else {
        // Todo Correcto, tomar valores del select y ejecutar la busqueda
        cotizador.obtenerValores(monedaSeleccionada, criptomonedaSeleccionada)
        .then((data) => {
            ui.mostrarResultado(data.resultado, monedaSeleccionada)
        })
        .catch( err => console.error(err) )
        console.log('Cotizar');
    }
    
    //Comprobar
    
    console.log('Enviando....');
}) 