const path = require('path');


module.exports = {
    entry: {
        index: './src/js/index.js',
        nosotros: './src/js/nosotros'
    },
    output: {
        filename:'[name].bundle.js',
        path: path.join(__dirname, 'dist')
    },
    devServer: {
        contentBase:path.join(__dirname, 'dist'),
        compress:true,
        port: 9000
    },
    module: {
        rules: [
            {
                test:  /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ["es2015"]
                    }
                }
            },
            {
                test: /\.css$/,
                loaders: [ 'style-loader', 'css-loader']
            },
            {
                test: /\.scss$/,
                loaders: [ 'style-loader', 'css-loader', 'sass-loader']
            }
        ]
    },
    optimization: {
        //Archivos de codigo comun
        splitChunks: {
            cacheGroups:{
                commons: {
                    //webpack
                    test: /[\\/]node_modules[\\/]/,
                    name: 'common',
                    chunks: 'all'
                }
            }
        }
    }
}