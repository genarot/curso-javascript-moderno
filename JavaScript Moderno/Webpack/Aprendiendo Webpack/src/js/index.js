import '../css/style.css';
import '../css/estilos.scss'

const lista =  ['carro', 'perro','laptop','moto'];
 
let parrafo = document.createElement('p');
parrafo.innerText = 'Soy un parrafo de ejemplo';

class Cliente {
    constructor( nombre) {
        this.nombre = nombre;
    }
}

const cliente = new Cliente('Genaro');
console.log(cliente);


document.addEventListener('DOMContentLoaded', ( evt ) => {
    document.body.appendChild(parrafo);
    console.log(lista);
    
    document.getElementById('app').appendChild(document.createTextNode('Bienvenido al inicio'))
    
    const clientes = ['Cliente 1','Cliente 2', 'Cliente 3', 'Cliente 4'];
    
    let html ='';
    
    clientes.forEach( client => {
        html +=    `
            <li>${client}</li>
        `;
    });
    
    document.querySelector('#clientes').innerHTML = html;
})
