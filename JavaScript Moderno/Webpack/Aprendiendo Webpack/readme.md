# Aprendiendo Webpack
Pasos para crear nuestro primer bundle

## 1. Instalar las npm & node
Proceder a descargar e instalar npm y node js. (Omitir si ya estan instalados) 
# hello, This is Markdown Live Preview

## 2.  Iniciar un proyecto npm
Ejecutar el siguiente comandos 
`npm init`
y configurar

## 3. Instalar dependencias
Debemos instalar las dependencias, en este caso **webpack** y **webpack-cli**

`npm install webpack`

`npm install webpack-cli`

## 4. Crear primeros archivos
Crear el directorio **src** que contendra **index.js**
el cual puede contenter cualquier funcionalidad.

## 5. Creacion del bundle
En esta parte tendremos dos opciones, creacion por comandos o creacion mediante archivo de configuracion.

### a. Creacion mediante comandos

En esta parte procederemos a crear nuestro primer bundle utilizando comandos y nuestro archivo de referencia creado.
 
##### Ejecutar el sig comando
 
`node_modules/.bin/webpack src/main.js`

El cual nos creara la carpeta **dist** que contendra nuestro bundle llamado **index.js**

### b. Creacion mediante archivo de configuracion 

En esta parte procederemos a crear un bundle utilizando el archivo de configuraciones

##### Crear archivo de configuraciones

Debemos crear un archivo llamado `webpack.config.js` en nuestro directorio raiz de trabajo con la sig estructura

> module.exports = {
    entry: 'archivoEntrada.js',
    output: {
        filename: 'nombreDelArchivo',
        path: 'Ruta donde se creara el archivo'
    }
}

##### Agregar comandos personalizados a nuestro package.json

>  "scripts": {
    "ejecutar": "webpack --mode development", 
    "watch": "webpack --watch --mode development" `Espera cambios`
  },

# Transpilando con babel

## Instalar dependencias

Procedemos a instalar babel con el sig comando

`npm install --save-dev babel-core babel-loader babel-preset-env`

`npm install --save-dev babel-preset-latest babel-preset-stage-2 babel-plugin-transform-class-properties`

> Babel loader es el loader de webpack

## Crear configurador de babel

Creamos el archivo llamado `.babelrc` el que contendra los preset al que queremos transpilar nuestro codigo

>{
    "presets": [
        "es2015"
    ],
    "plugins": [
        "transform-class-properties"
    ]
}

# Importar archivos css en webpack

`npm install css-loader style-loader`

# Importar archivos scss, sass en webpack

`npm install --save-dev sass-loader node-sass`

# Instalando dev server

`npm install --save-dev webpack-dev-server`
