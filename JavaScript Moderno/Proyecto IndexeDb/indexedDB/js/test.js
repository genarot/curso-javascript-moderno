let carro = 'asda';
var fn = function ( param1, param2 ) {
    console.info( this, param1, param2 );
}
console.log(this);
 
var newFn = fn.bind( {carro}, 'param1Fixed' );
 
fn( 'Hello', 'World' ); // Window Hello World
newFn( 'Goodbye', 'Lenin' ); // Object { /* console*/ } param1Fixed Goodbye