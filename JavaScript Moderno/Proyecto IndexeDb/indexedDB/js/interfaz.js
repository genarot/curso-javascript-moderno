export class Interfaz {
    constructor(){}

    listarCitas( lista, citas, deleteHandler ) {
        citas.forEach(cita => {
            this.insertarCita( lista, cita, deleteHandler );
        });
    }

    insertarCita( lista , cita, deleteHandler ) {
        // console.log(cita);
        
        if ( cita.key != -1 ) {
            // console.log(cita);
            let citaHTML = document.createElement('li');
            citaHTML.setAttribute('data-cita-id', cita.key);
            citaHTML.style.marginBottom = '2px'; 
            citaHTML.classList.add('list-group-item');
            citaHTML.innerHTML = `
                <p class="font-weight-bold">Mascota: <span class="font-weight-normal">${cita.mascota}</span></p>
                <p class="font-weight-bold">Cliente: <span class="font-weight-normal">${cita.cliente}</span></p>
                <p class="font-weight-bold">Telefono: <span class="font-weight-normal">${cita.telefono}</span></p>
                <p class="font-weight-bold">Fecha: <span class="font-weight-normal">${cita.fecha}</span></p>
                <p class="font-weight-bold">Hora: <span class="font-weight-normal">${cita.hora}</span></p>
                <p class="font-weight-bold">Sintomas: <span class="font-weight-normal">${cita.sintomas}</span></p>
            `;
            
            //Append a la lista padre
            lista.appendChild(citaHTML);

            //boton de borrar
            const botonBorrar = document.createElement('button');
            botonBorrar.classList.add('borrar', 'btn', 'btn-danger');
            botonBorrar.innerHTML = `<span aria-hidden="true">x</span> Borrar`;
            botonBorrar.onclick = deleteHandler;
            citaHTML.appendChild(botonBorrar)
        }
    }

    limpiarListaCitas( lista ) {
        while( lista.firstElementChild ) {
            lista.removeChild(lista.firstElementChild);
        }
    }

    mostrarErrorDb() {
        let divError = document.querySelector('#error-db');
        divError.innerHTML =   `
        <div class="alert alert-danger" style="display:block;">
                Ocurrio un error con la base de datos
        </div>
        `;
    }
}