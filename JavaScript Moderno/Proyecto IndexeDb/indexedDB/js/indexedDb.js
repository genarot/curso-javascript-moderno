export class Database {
    constructor( nombre, version ){
        this.DB         = null;
        this.nombre     = nombre;
        this.version    = version;
    }
    
    //Crear la base de datos
    async crearDatabase() {
        let { DB, nombre, version} = this;
        function creacionDb(resolve , reject) {
            
            if (!window.indexedDB) {
                window.alert("Su navegador no soporta una versión estable de indexedDB. Tal y como las características no serán validas");
                // return false;
            } else {
                let crearDb = window.indexedDB.open(nombre, version);
                
                //Si hay un error enviarlo a la consola
                crearDb.onerror = function( ev ) {
                    console.error('Hubo un error con la creacion de la base de datos Indexe');
                    console.error(this.error);
                    alert('Error en el indexeDb, algunas caracteristicas estaran deshabilitadas.')
                    reject( this.error );
                }
    
                //si todo esta bien entonces muestra en consola, y asignar la base de datos
                crearDb.onsuccess = function( evt ) {
                    
                    console.log(`Base de datos ${nombre} , version ${version} creada/lista.`);
                    //Asignar la base de datos
                    DB  = crearDb.result;

                    resolve( DB )
                }
                
                //Este metodo solo corre una vez y es ideal para crear el schema de la base.
                crearDb.onupgradeneeded = function(e) {
                    createSchemas( e.target.result );
                }
            }
        }
        return  new Promise(creacionDb);
    }

    setDB ( database ) {
        this.DB = database;
    }
    async nuevaCita( nuevaCita ) {
        //En indexedDb se utilizan transacciones
        try{
            let transaction = this.DB.transaction(['citas'], 'readwrite');
            let objectStore = transaction.objectStore('citas');
            // console.log(objectStore);
            let peticion = objectStore.add( nuevaCita );
            let insertar = await function (resolve, reject ) {
                // console.log(peticion);
                peticion.onsuccess = (evt) => {
                    if ( !evt.returnValue ) {
                        reject( {error: 'Ocurrio un error no se pudo agregar'})                  
                        console.warn(evt);
                    }
                }
                transaction.oncomplete = (evt) => {
                    console.log('Cita agregada');
                    resolve(evt)
                    // obtenerCitas();
                    // ui.insertarCita( citas, {})
                }
                transaction.onerror = () => {
                    console.error('Hubo un error');
                    console.error(this.error);
                    reject( this.error );
                }
            }
            return new Promise(insertar);
        } catch ( err ) {
            return await Promise.reject(err)    
        }
    }

    async obtenerCitas() {
        //creamos un objectstore
        // console.log(this.DB);
        console.log('Obteniendo Citas');
        
        let objectStore = this.DB.transaction(['citas'], 'readonly').objectStore('citas');
        let citasArray  = [];
        function citas( resolve, reject) {
            //esto retorna una pticion
            objectStore.openCursor().onsuccess = function(e) {
                //cursor se va a ubicar en el registro indicado para acceder a los datos
                let cursor = e.target.result;
                
                if ( cursor ) {            
                    let { key, value } =cursor;
                    Object.assign(value, {key}, value)
                    citasArray.push(value);
                    // ui.insertarCita( citas, value) 
                    
                    //Consultar los proximos registros
                    cursor.continue();
                } else {
                    resolve(citasArray)
                    
                }
            }
            objectStore.openCursor().onerror = function(e) {
                reject( this.error );
            }
        };
        return new Promise( citas );
    }

    async eliminarCita( id ) {
        let transaction = this.DB.transaction(['citas'], 'readwrite');
        let objectStore = transaction.objectStore('citas');
        let insertar    = function( resolve, reject ) {
            let request         = objectStore.delete(Number( id ) );
            // request.onsuccess   = function( evt ) {
            //     resolve( evt.isTrusted )
            // }
            transaction.oncomplete = function(evt) {
                console.log(evt)
                resolve(evt)
            }
            transaction.onerror     = function( evt ) {
                console.log('Error eliminando cita');
                reject(this.error)
            }
        }
        return new Promise(insertar);
    }
}
function createSchemas( database ) {
    //El evento es la misma base de datos
    let db = database;
            
    //Definir el objectstore, toma 2 parametros el nombre de la db y segundo las opciones
    //keyPath es el indice de la base de datos
    let objectStore = db.createObjectStore('citas', {keyPath: 'key', autoIncrement: true});

    //Crear los indices y campos de la base de datos, createIndex: 3 param nombre, keyPath y opciones
    objectStore.createIndex('mascota',  'mascota',  {unique: false});
    objectStore.createIndex('cliente',  'cliente',  {unique: false});
    objectStore.createIndex('telefono', 'telefono', {unique: false});
    objectStore.createIndex('fecha',    'fecha',    {unique: false});
    objectStore.createIndex('hora',     'hora',     {unique: false});
    objectStore.createIndex('sintomas', 'sintomas', {unique: false});
}