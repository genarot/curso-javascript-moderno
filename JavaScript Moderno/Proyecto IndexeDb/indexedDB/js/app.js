import {Interfaz} from './interfaz.js';
import {Database} from './indexedDb.js'
let ui  = new Interfaz();

(function(){
    let database;
    let DB;
    function deleteCitaHandler(e) {
        
        let citaID = e.target.parentElement.getAttribute('data-cita-id');
        database.eliminarCita( citaID ) 
        .then((val) => {
            console.log(val);
            
            obtenerCitas();
        })
        .catch((err) => {
            console.error(err);
            
        })
        console.log(citaID);
    }

    //Selectores de la interfaz
    let form    = document.querySelector('form'),
        nombreMascota       = form.querySelector('#mascota'),
        nombreCliente       = form.querySelector('#cliente'),
        telefono            = form.querySelector('#telefono'),
        fecha               = form.querySelector('#fecha'),
        hora                = form.querySelector('#hora'),
        sintomas            = form.querySelector('#sintomas'),
        citas               = document.querySelector('#citas'),
        headingAdministra   = document.querySelector('#administra');
    
    //Esperar por el DOM ready
    document.addEventListener('DOMContentLoaded', () => {
        //Una ves ready cargamos 
        database = new Database('citas', 1);

        database.crearDatabase()
        .then(( indexDb) => {
            DB = indexDb;
            database.setDB(indexDb);
            console.log(indexDb);
            
            obtenerCitas();
        })
        .catch((err) => {
            console.error(err);
            ui.mostrarErrorDb();
        })
            
        //Cuando el formulario se envia
        form.addEventListener('submit', agregarDatos);
    
    })
    function agregarDatos( e ) {
        e.preventDefault();
        
        const nuevaCita = {
            mascota: nombreMascota.value,
            cliente: nombreCliente.value,
            telefono: telefono.value,
            fecha: fecha.value,
            hora: hora.value,
            sintomas: sintomas.value
        };

        // console.log(database.nuevaCita);
        database.nuevaCita( nuevaCita )
        .then((result) => {
            form.reset();
            obtenerCitas();
        })
        .catch( err => {
            console.error(err);
            // alert('error eliminando')
            // console.error(err);
            
        })
        
    }
    function obtenerCitas() {
        // Limpiar las citas
        ui.limpiarListaCitas(citas);
    
        database.obtenerCitas(DB)
        .then( arrayCitas => {
            console.log(arrayCitas);
            ui.listarCitas(citas, arrayCitas, deleteCitaHandler)
            if ( !citas.firstElementChild ) {
                // Cuando no hay registros
                headingAdministra.textContent = 'Agregar citas para comenzar';
                let listado = document.createElement('p');
                listado.classList.add('text-center');
                listado.textContent = 'No hay registro';
                citas.appendChild(listado)
                
            } else {
                headingAdministra.textContent = 'Administra tus citas';
            }
        })
        .catch( err => {
            alert('error insertando')
            // console.error(err)
        })
    }
    
})();