// variables
const cargarPosts = document.querySelector('#cargar')
    .addEventListener('click', cargarAPI);

function cargarAPI () {
    // crear objecto
    const xhr = new XMLHttpRequest();

    //Abrir conexion
    xhr.open('GET', 'https://jsonplaceholder.typicode.com/posts', true);

    //Carga y leer datos
    xhr.onload = function(evt) {
        if ( this.status === 200 ) {
            console.log(JSON.parse(this.responseText) );
            const posts = JSON.parse(this.responseText);
            
            let contenido = '';
            posts.forEach(post => {
                contenido +=    `
                    <div  class="card" style="margin-bottom:5px;">
                        <div class="card-header">
                            <strong>${post.title}</strong>
                        </div>
                        <div class="card-body">
                            <p>${post.body}</p>
                        </div>
                    </div>
                `
            });
            document.getElementById('listado').innerHTML = contenido; 
        }  
    };

    //enviar la conexion
    xhr.send()
}