// variables
const boton1 = document.getElementById('boton1');
const boton2 = document.getElementById('boton2');





// Event Listener
boton1.addEventListener('click',(evt) => {
    const xhr = new XMLHttpRequest();
    console.log('Obteniendo empleado');
    
    xhr.open( 'GET', './empleado.json', true );
    
    xhr.onload =  function() {
        console.log(this);
        
        if ( this.status === 200 ) {
            const persona = JSON.parse(this.responseText );
            const htmlTemplate =   `
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    Empleado Obtenido
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><span>Nombre:</span> ${persona.nombre}</li>
                    <li class="list-group-item"><span>Empresa:</span> ${persona.empresa}</li>
                    <li class="list-group-item"><span>Cargo:</span> ${persona.trabajo}</li>
                </ul>
            </div>
            `;
            document.getElementById('empleado').innerHTML = htmlTemplate;
        }  
    };

    xhr.send();
})

boton2.addEventListener('click', (evt) => {
    const xhr = new XMLHttpRequest();

    xhr.open('GET','./empleados.json', true);

    xhr.onload =  function(evt) {
        if ( this.status === 200 ) {
            const empleados = JSON.parse(this.responseText);
            console.log(empleados);
            let htmlTemplate = '';
            empleados.forEach(empleado => {
                htmlTemplate += `<div class="card">
                    <div class="card-header text-white bg-primary mb-3">
                        Empleado Obtenido
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><span>Nombre:</span> ${empleado.nombre}</li>
                        <li class="list-group-item"><span>Empresa:</span> ${empleado.empresa}</li>
                        <li class="list-group-item"><span>Cargo:</span> ${empleado.trabajo}</li>
                    </ul>
                </div>`;
            });
            document.getElementById('empleados').innerHTML = htmlTemplate;
        }
    };

    xhr.send();
});