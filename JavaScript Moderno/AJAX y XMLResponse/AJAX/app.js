document.getElementById('cargar').addEventListener('click', cargarDatos);

function cargarDatos(){
    // Crear el objeto xmlhttRquest
    const xhr = new XMLHttpRequest();
    
    //Abrir una conexion
    xhr.open('GET', './datos.txt', true);

    // Forma Vieja
    /*  
    *   0 -  No inicializado
    *   1 - Conexion establecida
    *   2 - Recibido
    *   3 - Procesando
    *   4 - Respuesta lista
    */
    // xhr.onreadystatechange = function() {
    //     if ( this.readyState === 4 && this.status === 200) {
    //         document.getElementById('listado').innerHTML = `<h2> ${this.responseText}</h2>`;
    //     }        
    // }

    // Forma nueva
    // una vez que carga
    xhr.onload = function(evt) {
        // 200 : Correcto | 403 :  prohibido  | 404: No encontrado
        if ( this.status === 200 ) {
            console.log(this.responseText);
            document.getElementById('listado').innerHTML = `<h2> ${this.responseText}</h2>`;
        }
    }

    //Enviar el request
    xhr.send();
}