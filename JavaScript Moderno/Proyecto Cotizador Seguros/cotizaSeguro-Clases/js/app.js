'use strict'
class Seguro {
    // Constructor para seguro
    constructor( marca, anio, tipo ) {
        this.marca  = marca;
        this.anio   = anio;
        this.tipo   = tipo;
    }

    // se usa this por que corresponde a la instancia
    cotizarSeguro() {
        /**
         *  1 = Americano 1.15
         *  2 = Asiatico 1.05
         *  3 = Europeo 1.35
         */
        let     cantidad ;
        const   base  = 2000;

        switch( this.marca ) {
            case '1':
                cantidad = base * 1.15;
                break;
            case '2':
                cantidad = base * 1.05;
                break;
            case '3':
                cantidad = base * 1.35;
                break;
        }

        //Leer el anio
        const diferencia    = new Date().getFullYear() - this.anio;
        // Cada año de diferencia hay que reducir el 3% el valor del seguro
        cantidad -= (diferencia*3) * cantidad/100;
        /*
            Si el seguro es basico se multiplica por 30% mas
            Si el seguro es completo 50% mas
        */
        if ( this.tipo === 'basico' ) {
            cantidad *= 1.3;
        } else {
            cantidad *= 1.5;
        }
        
        return cantidad;
    }
}

// Todo lo que se muestra
class Interfaz{
    
   mostrarMensaje( mensaje, tipo ) {
        const div = document.createElement('div');
        div.className = 'alert';
        if ( tipo === 'error') {
            div.classList.add('alert-danger');
        } else {
            div.classList.add('alert-primary');
        }
        div.setAttribute('role','alert');
        // console.log(div.outerHTML);
        div.innerHTML = `
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>`;
        div.appendChild( document.createTextNode(mensaje) );
    
        // console.log(document.querySelector('#cotizar-seguro'));
        
        formulario.insertBefore(div, formulario.querySelector('.form-group'))
    
        setTimeout(function(){
            document.querySelector('.alert').remove();
        },3000);
    }
    
    mostrarResultado( informacion,total ) {
        const resultado = formulario.querySelector('#resultado' );
        const spinner  = formulario.querySelector('#cargando img' );
        spinner.style.display = 'block';
        let marca;
        switch( informacion.marca ) {
            case '1':
                marca   = 'Americano';
                break;
            case '2':
                marca   = 'Asiatico';
                break;
            case '3':
                marca   = 'Europeo';
                break;
        }
        // Crear un div
        const div   = document.createElement('div');
        div.classList.add('card')
        div.style.marginTop = '5px';
        //Insertar Informacion
        div.innerHTML = 
        `
            <h5 class="card-header bg-info mb-3 text-white">Tu Cotización</h5>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Marca:</strong> ${marca}</li>
                <li class="list-group-item"><strong>Año:</strong>${informacion.anio}</li>
                <li class="list-group-item"><strong>Tipo:</strong>${informacion.tipo}</li>
                <li class="list-group-item"><strong>Total:</strong>${total}</li>
            </ul>
        `;
        setTimeout(() => {
            spinner.style.display = 'none';
            resultado.appendChild(div);
        }, 3000);
    };
};

//Mensaje que se imprime en el html
// Interfaz.prototype.mostrarError = function ( mensaje ) {
//     const div = document.createElement('div');
//     div.className = 'alert alert-danger';
//     div.setAttribute('role','alert');
//     div.appendChild( document.createTextNode(mensaje) );
//     document.querySelector('.container')
//         .appendChild(div);
// }


//Event Listeners
const formulario  = document.getElementById('cotizar-seguro');
formulario.addEventListener('submit', function(evt){
    evt.preventDefault();

    // Leer la marca seleccionada del select
    const marca = document.getElementById('marca');
    const marcaSelect = marca.options[marca.selectedIndex].value;
    // console.log(marcaSelect);

    //leer el anio seleccionado del select
    const anio  = document.getElementById('anio');
    const anioSelect    = anio.options[anio.selectedIndex].value;

    //lee el valor del radio button
    const tipo  = document.querySelector('input[name="tipo"]:checked').value;
    
    // console.log(tipo);
    
    //Crear instancia de interfaz
    const interfaz  = new Interfaz();
    
    //REvisamos que los campos no esten vacios 
    if ( marcaSelect === '' || anioSelect === '' || tipo === '') {
        // Interfaz imprimiendo un error
        console.warn('Faltan Datos');
        interfaz.mostrarMensaje('Faltan Datos, revisar el formulario y muestra de nuevo','error');
    } else {
        //LImpiar resultados anteriores
        let resultado = formulario.querySelector('#resultado');
        while( resultado.firstChild ) {
            resultado.removeChild( resultado.firstChild )
        }

        // Instanciar seguro y mostrar interfaz
        const seguro = new Seguro( marcaSelect, anioSelect, tipo);
        console.log(seguro);
        // Cotizar el seguro
        const cantidad = seguro.cotizarSeguro();
        // Mostrar el resultado
        interfaz.mostrarResultado( seguro, cantidad );
        interfaz.mostrarMensaje('Cotizando...')
    }
})

const max = new Date().getFullYear(),
    min = max - 20;

console.log(max);
console.log(min);

const selectAnios = document.getElementById('anio');

for ( let i = max; i >= min; i-- ){
    let option  = document.createElement('option');
    option.appendChild(document.createTextNode(i));
    option.value = i;
    selectAnios.appendChild(option);
}