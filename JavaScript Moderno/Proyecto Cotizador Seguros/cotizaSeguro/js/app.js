// Constructor para seguro
function Seguro(marca, anio, tipo) {
    this.marca= marca;
    this.anio   = anio;
    this.tipo   = tipo;
}

// Todo lo que se muestra
function Interfaz(){};

//Event Listeners
const formulario  = document.getElementById('cotizar-seguro');
formulario.addEventListener('submit', function(evt){
    evt.preventDefault();

    // Leer la marca seleccionada del select
    const marca = document.getElementById('marca');
    const marcaSelect = marca.options[marca.selectedIndex].value;
    // console.log(marcaSelect);

    //leer el anio seleccionado del select
    const anio  = document.getElementById('anio');
    const anioSelect    = anio.options[anio.selectedIndex].value;

    //lee el valor del radio button
    const tipo  = document.querySelector('input[name="tipo"]:checked').value;

    console.log(tipo);
    
    // Crear instancia de Interfaz
    const interfaz = new Interfaz();

    //Revisamos que los campos no esten vacios
    if ( marcaSelect === ''  )
    console.log('Presionado');
})

const max = new Date().getFullYear(),
    min = max - 20;

console.log(max);
console.log(min);

const selectAnios = document.getElementById('anio');

for ( let i = max; i >= min; i-- ){
    let option  = document.createElement('option');
    option.appendChild(document.createTextNode(i));
    option.value = i;
    selectAnios.appendChild(option);
}