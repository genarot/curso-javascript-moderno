class Interfaz {
    constructor() {
        //Inicializar y obtener las propiedades del mapa;
        this.init();
    }
    init () {
        this.inicializarMapa();
    }
    
    inicializarMapa() {
        let latLng = {lat: 19.427536, lng:-99.134096}
        this.mapa = new google.maps.Map(document.getElementById('mapa'), {
            center: latLng,
            zoom: 7.3
        })
    
        this.api = new API();
    } 
    mostrarEstablecimientos() {
        this.api.obtenerDatos()
        .then( data => {
            let {results} = data;
            
            //mostrar los pines en el mapa
            this.mostrarMapa(results);
        } )
        .catch( err => console.error( err ) )
    }

    mostrarMapa( datos ) {
        // //Almacena InfoWindow Activo
        let infoWindowActivo;

        // console.log(datos);
        datos.forEach( gasolinera => {
            console.log(gasolinera);
            //Destructuring
            let {calle, latitude, longitude, premium, regular } = gasolinera;
            
            //Crear objeto con la latitud y longitud
            let latLng = {
                lat: Number(latitude),
                lng: Number(longitude)
            }

            //Agregar el pin
            let marker = new google.maps.Marker({
                position: latLng,
                map: this.mapa,
                title: calle
            })

            let infoWindow = this.crearInfoWindow(calle, regular, premium);
            
            marker.addListener('click', () => {
                //Cerrar el infowindow activo
                if ( infoWindowActivo ) {
                    infoWindowActivo.close()
                }

                //Mostrarlo
                infoWindow.open(this.mapa, marker);

                //Asignar el activo 
                infoWindowActivo= infoWindow;
            })
        });
    }

    //Crear el infoWindow
    crearInfoWindow( calle, regular, premium) {
        return new google.maps.InfoWindow({
            content: `
            <div id="infoSucActivo">
                <h3 id="firstHeading" class="">${calle}</h3>
                <p><span style="font-weight:bold">Precio Regular:</span>${regular}</p>
                <p><span style="font-weight:bold">Precio Premium:</span>${premium}</p>
            </div>   
            `
        })
    }

    obtenerSugerencias( calle  ) {
        this.api.obtenerDatos()
        .then( (data) => {
            //Obtener los resultados
            const resultados = data.results;
            
            return this.filtrarSugerencias( resultados, calle )
        })
        .catch( err => console.error(err) )
    }

    filtrarSugerencias( resultados, calle ) {
        // console.log(resultados);
        
        const filtro = resultados.filter( filtro => filtro.calle.toLowerCase().indexOf(calle) != -1);
        console.log(filtro);
        this.inicializarMapa();
        this.mostrarMapa(filtro);
    }  
}