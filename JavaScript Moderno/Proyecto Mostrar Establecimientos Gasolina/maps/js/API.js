class API {
    async obtenerDatos() {
        //Obtener desde la API
        const datos = await fetch('https://api.datos.gob.mx/v1/precio.gasolina.publico');

        //Retornar como JSON
        const respuesta = await datos.json();

        //Retornar el objeto
        return respuesta;
    }

    // async obtenerSugerencias( calle ) {
    //     //Obtener desde la API
    //     const data  = await fetch('https://api.datos.gob.mx/v1/precio.gasolina.publico');

    //     //Retornar como JSON
    //     const respuesta = await data.json();

    //     //Retornar Ejemplo
    // }
}