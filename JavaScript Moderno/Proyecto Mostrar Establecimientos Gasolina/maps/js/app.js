const ui    = new Interfaz();
// const api   = new API();

// Cuando cargue el DOM
document.addEventListener('DOMContentLoaded', evt => {
    console.log('El DOM ya cargo');
    //
    ui.mostrarEstablecimientos();
})
// console.log(api.obtenerDatos());

// api.obtenerDatos()
// .then( data => {
//     console.log(data);
    
// })
// .catch(err => console.error(err) )

//Habilitar la busqueda en vivo
const buscador = document.querySelector('#buscar input');

buscador.addEventListener('input', (evt) => {
    //Si es mayor a 3 buscar sugerencias
    let valor = buscador.value;
    if ( valor.length > 3 ) {
        ui.obtenerSugerencias(valor);
    }  else if (valor.length === 0 ){
        ui.inicializarMapa();
        ui.mostrarEstablecimientos();
    }   else {
        console.warn('Ingresa mas caracteres');
    }
    
})