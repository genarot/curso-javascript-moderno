import { nombreCliente, ahorro, Cliente, mostrarInformacion } from './cliente.js'; //importar variables
import {nombreEmpresa, ahorro as ahorroEmpresa, categoria, mostrarInformacion as mostrarInfoEmpresa, Empresa} from './empresa.js'
// import * as cliente from './cliente.js' //importar todo con alias

// console.log(cliente);

// console.log(nombreCliente);
// console.log(ahorro);

//Importacion con alias
// console.log(cliente.aho666rro);

// console.log(mostrarInformacion());

// console.log(cliente.mostrarNombre(cliente.nombreCliente));

//Usando la clase
const cliente = new Cliente('Genaro','Tinoco', 'Programador');

console.log(cliente);
console.log(cliente.mostrarInformacion());



console.log(nombreEmpresa);
console.log(ahorroEmpresa);
console.log(categoria);
console.log(mostrarInfoEmpresa(nombreEmpresa, ahorroEmpresa, categoria));

let empresa1 = new Empresa('Casa Cross', 120, 'Repuestos');
console.log(empresa1);
