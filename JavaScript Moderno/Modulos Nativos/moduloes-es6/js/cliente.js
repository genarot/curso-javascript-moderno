//Exportar variables
export const nombreCliente = 'Genaro';
export let ahorro = 1200;

//Exportar funciones
export function mostrarInformacion( nombre, ahorro) {
    return `Cliente: ${nombre} ,  ahorro: ${ahorro}`;
} 

//Exportar funciones
export function mostrarNombre( nombre ) {
    return `NOmbre del cliente: ${nombre}`
}

//Exportar una clase
export class Cliente {
    // constructor(nombre, apellido, cargo) {
    //     this.nombre = nombre;
    //     this.apellido = apellido;
    //     this.cargo = cargo;
    // }
    constructor(nombre, ahorro) {
        this.nombre =nombre;
        this.ahorro = ahorro;
    }

    mostrarInformacion() {
        return `Nombre ${this.nombre} , apellido: ${this.apellido}, cargo: ${this.cargo}`
    }
}