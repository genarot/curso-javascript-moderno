import {Cliente} from './cliente.js'

//Exportar variables
export const nombreEmpresa = 'AtomicDev';
export let ahorro = 20000000000;
export const categoria = 'Desarrollo';

//Exportar funciones
export function mostrarInformacion( nombre, ahorro, categoria) {
    return `Nombre Empresa: ${nombre}, ahorro: ${ahorro}, categoria: ${categoria}`
}

export class Empresa extends Cliente {
    constructor( nombre, ahorro, categoria) {
        super(nombre, ahorro);
        this.categoria = categoria;
    }

    mostrarInformacion( ) {
        return `Nombre Empresa: ${this.nombre}, ahorro: ${this.ahorro}, categoria: ${this.categoria}`
    }
}