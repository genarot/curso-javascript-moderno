const exp1 = new RegExp('/abc');
const exp2 = /abc/;

console.log(exp1);
console.log(exp2);

let valor, expReg;

expReg= /[0-9]/
valor = 19892;

//Una fecha en exp regular 20-10-2018
expReg = /\d\d-\d\d-\d\d\d\d/
valor = '20-10-2018'

//Hora
expReg = /\d\d:\d\d/
valor = '10:48'

// \d caracter numerico
// \D caracter no numerico
expReg = /\d\d:\d\d [AM|PM]/
valor = '10:48 AM'

expReg = /\d+/
valor = '1212'

//  ^ negacion
expReg = /[^0-9]/

//fecha
expReg = /[0-9]{1,2}-\d{1,2}-\d{1,4}/
valor   = '10-10-2018'
valor = '1-1-2018';
valor = '234-21-2018';
valor = '12-12-20'

//letras o numeros
expReg = /\w+/
valor = 'Mensaje de pruebas'
valor = '232'
valor = ' '

// Puras mayusculas
expReg  = /([A-Z])\w+/
valor   = 'HOLA s'

console.log(expReg.test(valor));
