// MAPS
let cliente = new Map();
cliente.set('nombre','Genaro');
cliente.set('tipo','Premium');
cliente.set('saldo',3000);

//Acceder a los valores
console.log(cliente.get('nombre'));
console.log(cliente.get('tipo'));
console.log(cliente.get('saldo'));

//Metodos de los Maps
//tamano del map
console.log(cliente.size);
//Comprobar si una llave existe
console.log(cliente.has('apellido'));
console.log(cliente.has('nombre'));


//borrar un elemento del map
cliente.delete('nombre')

console.log(cliente);

//Otro ejemplo
let paciente = new Map(
    [['nombre','paciente'],
     ['habitacion', 'No definido']]
); //por default

paciente.set('nombre','Antonio');
paciente.set('habitacion', 400);
paciente.set('habitacion', 600);//sobre escribe

//for each map
paciente.forEach( (datos, key) => {
    console.log(`${key} : ${datos}`);
    
})
console.log(paciente);
