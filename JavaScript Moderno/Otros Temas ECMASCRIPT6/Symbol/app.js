//SYMBOL

const simbolo1 = Symbol();
const simbolo2 = Symbol('Descripcion aqui');

console.log(simbolo1);
console.log(simbolo2);


let nombre = Symbol();
let apellido = Symbol();

//Creamos una persona
let persona = {};
persona.nombre = 'Genaro';
persona[nombre] = 'Jose';
persona.apellido = 'Tinoco';
persona[apellido] = 'Palacios';
persona.saldo = 2323232;
persona.tipoCliente = 'Gold';

//Las propiedades symbol son privadas no pueden ser accedidas de la sig manera
for( key in persona ) {
    console.log(`${key} : ${persona[key]}`);
}

console.log(persona);
