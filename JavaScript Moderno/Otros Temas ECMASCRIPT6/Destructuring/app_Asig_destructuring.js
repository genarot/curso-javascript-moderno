// Destructuring
//Asignacion Destructuring
const cliente = {
    nombre: 'Genaro',
    tipo: 'Premium'
};

//Forma normal
// let nombre = cliente.nombre,
//     tipo = cliente.tipo;

let nombre= 'Jose',
    tipo= 'Gold';
//Destructuring Normal
// let {nombre, tipo} = cliente;

//Si ya estan definidas las variables , por default igual que en funciones
({nombre, tipo, saldo = 0} = cliente)

console.log(nombre);
console.log(tipo);
console.log(saldo);


const trabajador = {
    cargo: 'Programador',
    nombre: 'Genaro', 
    tinoco: 'Tinoco',
    datos: {
        ubicacion:{
            ciudad: 'Nagarote',
            pais: 'Nicaragua'
        },
        cuenta: {
            desde: '10-12-2012',
            saldo: 4000
        }
    }
}

let {datos: {ubicacion, ubicacion: {ciudad}}}  = trabajador;

console.log(ubicacion);
console.log(ciudad);

let {datos: {cuenta} } = trabajador;
console.log(cuenta);
