//Destructuring Arreglos y Objetos 
const ciudades = ['Leon','Managua','Granada','Masaya', {idioma: 'ingles'}];

//en orden
const [primeraCiudad,  segundaCiudad] = ciudades;

//Ultima
const [, , madrid, ] = ciudades;

console.log(primeraCiudad);
console.log(segundaCiudad);
console.log(madrid);

//Ejemplo Avanzado
const cliente = {
    tipo: 'Premium',
    saldo: 30000,
    datos: {
        nombre: 'Genaro',
        apellido: 'Tinoco',
        residencia: {
            ciudad: 'Nagarote'
        }
    },
    movimientos: ['12-03-2017', '25-10-2018','12-12-2018']
}

let {datos: {residencia}, tipo, movimientos: [primerMovimiento]} = cliente;

console.log(residencia);
console.log(tipo);
console.log(primerMovimiento);

//Destructuring funcion
function reservacion(completo,
        {
            metodoPago = 'efectivo',
            cantidad = 0,
            dias = 0
        } = {}){

        if ( completo ) {
            console.log(`Proceder a reservar, ${cantidad} habitaciones por ${dias} dias pagando en ${metodoPago}`);
        }else {
            console.warn('Abandonar');
            
        }
}

//
reservacion(
    true,
    {
        metodoPago: 'Tarjeta',
        dias: 7
    }
)