//Un generador es una funcion que va a retornar un iterador
function *crearGenerador() {
    //yield
    yield 1;
    yield 'Nombre';
    yield 3+3;
    yield true;
}

const iterador = crearGenerador();

console.log(iterador.next().value);
console.log(iterador.next().value);
console.log(iterador.next().value);
console.log(iterador.next().value);
console.log(iterador.return());

//Generador para arreglo
function *nuevoGenerador( carrito ) {
    for ( let i= 0; i < carrito.length; i++ ) {
        yield carrito[i];
    }
}
//creamos el carrito 
const carrito = ['Camisa','Pantalon','Mochila'];

//Recorremos el iterador
let iteradorCarrito = nuevoGenerador(carrito);
console.log(iteradorCarrito.next().value);
console.log(iteradorCarrito.next().done);
console.log(iteradorCarrito.next().value);
console.log(iteradorCarrito.next().done);
