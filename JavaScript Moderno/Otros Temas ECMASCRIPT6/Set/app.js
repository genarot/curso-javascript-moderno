let carrito = new Set();
//Como los set java, son elementos unicos
carrito.add('Camisa');
carrito.add('Pantalon');
carrito.add('Disco #1');
carrito.add('Camisa2');
carrito.add('Camisa');

let numeros = new Set([1,2,3,4,7,8,3,1,23,4,2,5])

//Comprobar que un valor exista
console.log(carrito.has('Camisa'))

//limpiar 
// carrito.clear();

console.log(numeros);

console.log(carrito);
console.log(carrito.size);

//iterar
carrito.forEach( (producto, index) => {
    console.log(`${index} : ${producto}`);
    
})

//Convertir Set a arreglo
const arreglo = Array.from(carrito);
const arregloCarrito = [...carrito];

console.log(arreglo);
console.log(arregloCarrito);