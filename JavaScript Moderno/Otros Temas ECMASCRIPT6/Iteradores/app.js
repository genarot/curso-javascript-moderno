function crearIterador( carrito ) {
    //Inicializamos el indice
    let i = 0;

    return {
        siguiente: () => {
            let fin = (i >= carrito.length);
            
            let valor = !fin ? carrito[i++] : undefined;

            return {
                fin: fin,
                valor: valor
            }
        }
    }
}

const carrito = ['producto 1', 'producto 2', 'producto 3','producto 4'];

const recorrerCarrito = crearIterador(carrito);

console.log(recorrerCarrito.siguiente());
console.log(recorrerCarrito.siguiente());
console.log(recorrerCarrito.siguiente());
console.log(recorrerCarrito.siguiente());
console.log(recorrerCarrito.siguiente());
