//En javascrup existe un objecto llamado DAte
const diaHoy = new Date();

console.log(diaHoy)
console.log(diaHoy.toLocaleString())

let valor;
//Mes
valor = diaHoy.getMonth();
//Dia 
valor = diaHoy.getDate();
valor = diaHoy.getDay(); //dia de la semana
//Obtener Año
valor   = diaHoy.getFullYear();
//Minutos
valor   = diaHoy.getMinutes();
// Hora
valor   = diaHoy.getHours();

// Milisegundos desde 1970
valor   = diaHoy.getTime();

console.log(valor);