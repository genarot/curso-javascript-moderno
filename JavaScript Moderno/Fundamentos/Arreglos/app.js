//Creacion Arreglos
const numeros = [10, 20, 30, 40, 50,60];

const meses = new Array('Enero', 'Febrero','Marzo','Abril');

// Anadir elemento
meses[4] = 'Mayo';
meses.push('Junio');

console.log(meses)
//Anadir al inicio del arreglo
meses.unshift('Mes 0');

console.log(meses)

//Eliminar el ultimo elemento de un arreglo
meses.pop();
//Eliiniar el primer elemento de un arreglo
meses.shift();
console.log(meses)
//QUitar un rango
meses.splice(1, 2)
console.log(meses)

//Revertir arreglo
meses.reverse();
console.log(meses)


//encontrar un  elemento 
console.log(meses.indexOf('Abril'))

let arreglo1 = [12,23,45],
    arreglo2 = ['a','c','b'];

console.table(arreglo1);
console.table(arreglo2);
console.table(arreglo1.concat(arreglo2))

// Ordenar un arreglo 
const frutas = ['Platano','Fresa','Manzana','Zanahoria','Naranja'];
console.warn('Arreglo desordenado');
console.table(frutas);
console.warn('Arreglo Ordenado');
console.table(frutas.sort())

//Ordenar numeros
let arrNum = [2,45,5,24,56,35,9,94,12,3,457,999,510];
console.table( arrNum.sort() );

console.table(arrNum.sort((a, b) => {
    return a - b;
}))

console.table(arrNum.sort((x, y) => {
    return y - x;
}))