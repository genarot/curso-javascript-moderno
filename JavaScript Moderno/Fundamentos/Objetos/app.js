const persona = {
    nombre: 'Genaro',
    apellido: 'Tinoco',
    profesion: 'Programador',
    email:'genaro_13@hotmail.es',
    edad: 22,
    hogar:{
        ciudad: 'Nagarote',
        pais: 'Nicaragua'
    },
    telefonos:[
        '23132362',
        '75311183'
    ],
    fechaNac: function(){
        return new Date().getFullYear() - this.edad;
    }
}

console.table(persona)
console.log(persona)

persona.telefonos.push('112323')
console.table(persona)

console.log(persona.fechaNac())


console.warn('Arreglo de Objetos');
const autos = [
    {marca:'Toyota', modelo:'Hylux',    anio:'2018'},
    {marca:'Ford',   modelo:'Mustang',  anio:'2019'},
    {marca:'Toyota', modelo:'Yaris',    anio:'2017'}
];

console.log(autos)
console.warn('Mis autos');
for(let i= 0; i< autos.length; i++) {
    console.log(autos[i])
}