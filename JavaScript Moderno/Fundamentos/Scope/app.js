//Variables globales
var a = 'a';
var b = 'b';
const c = 'c';

//scope de la funcion
function funcionScope(){
    var a = 'A';
    let b = 'B';
    const c = 'C';
    console.log('Funcion: ',a,b,c);
}
funcionScope();

//scope de bloque{}
if (true) {
    var a = 'AA';
    let b = 'BB';
    const c = 'CC';
    console.log('Bloque: ',a,b,c);
}

//for
for (let a = 0; a < 10; a++) {
    console.warn(a)    
}

console.log('GLOBALES: ',a,b,c);