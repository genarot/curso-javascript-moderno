function sumar( x, y ) {
    return x + y;
}

console.log(` 4 + 6 = ${sumar( 4, 6)}`)

function saludar( nombre = 'Genaro'){
    console.log(`Bienvenido ${nombre}`)
}
saludar();
saludar('Prro');

//IIFE
//Auto invocadas
(function( tecnologia){
    console.log(`Aprendiendo ${tecnologia}`);
})('JavaScript');

//Metodos de propiedad
//CUando una funcion s epone dentro de un objecto
const musica = {
    reproducir: function(){
        console.log(`Reproduciendo musica`);
    },
    pausar: function(artista = 'Daddy Yankee'){
        console.log(`Pause la musica de ${artista}`);
    }
}

//Los metodos tambien pueden guardarse / crearse fuera del objeto
musica.borrar = function(id) {
    console.log(`Borrando la cancion con el ID: ${id}`);
}

musica.reproducir()

setTimeout(musica.pausar, 1200, 'Ozuna')
musica.borrar();

//FUncion que no existe
try{
    algo();
} catch(error) {
    console.error(error);
} finally {
    console.log('No le importa, ejecute siempre');
}
obtenerClientes();
function obtenerClientes() {
    console.log('Descargando...');

    setTimeout(function(){
        console.log('Completo')
    }, 3000);
}