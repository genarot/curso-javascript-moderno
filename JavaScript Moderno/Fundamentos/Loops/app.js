//Recorrer un arreglo con forEach
const pendientes = ['Tarea','Comer','Proyecto','Aprender JavaScript'];

//forEach
pendientes.forEach(function(pendiente, index){
    console.log(`${index} - ${pendiente}`)
})

const carrito = [
    {id:1, producto:'Libro'},
    {id:1, producto:'Camisa'},
    {id:1, producto:'Guitarra'},
    {id:1, producto:'Disco'}
];

const nombreProducto = carrito.map( function(carrito){
    return carrito.producto;
});

console.log(nombreProducto)

const automovil =  {
    marca: 'Toyota',
    modelo:'Hylux',
    anio:2018,
    motor:3.0
};

for ( let llave in automovil ) {
    console.log(`${llave} : ${automovil[llave]}`)
}