// String
const nombre1 = 'Pedro';
const nombre2 = new String('Pedro');

console.log(typeof nombre1);
console.log(nombre2);

// Al comparara un String con un Object String
// de manera estricta son diferentes por sus tipos de datos
// 'hola' > string, new('hola') > object
if ( nombre1 === nombre2){
    console.log('si, son iguales');
    
} else {
    console.log('no, son distintos');
    
}

//numeros
const numero1 = 20;
const numero2 = new Number(20);

console.log(numero1);
console.log(numero2);

//boolean
const boolean1 = true;
const boolean2 = new Boolean(true);

console.log(boolean1);
console.log(boolean2);

// funciones
const funcion1 = function (a,b) {
    return a+b;
}

const funcion2 = new Function('a','b','return a+b');

console.log(funcion1);
console.log(funcion2);

// Objectos
const persona1 = {
    nombre:'Genaro'
}

const persona2 = new Object({nombre: 'Genaro'}); 

//Arreglos 
const arreglo1 = [1,2,3,4];
const arreglo2 = new Array(1,2,3,4);