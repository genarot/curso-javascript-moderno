// Object Literal
const cliente = {
    nombre: 'Genaro',
    saldo: 3000,
    tipoCliente : function() {
        let tipo;
        if ( this.saldo > 1000 ) {
            tipo = 'Gold';
        }  else if ( this.saldo > 500 ) {
            tipo = 'Platinum';
        }
        else {
            tipo = 'Normal'
        }
        return tipo;
    }
}

console.log(cliente.tipoCliente())

// Metodo alternativo (Viejo)
function Cliente2(nombre, saldo ) {
    this.nombre = nombre;
    this.saldo = saldo;
    this.tipoCliente = function() {
        if ( this.saldo > 1000 ) {
            tipo = 'Gold';
        }  else if ( this.saldo > 500 ) {
            tipo = 'Platinum';
        }
        else {
            tipo = 'Normal'
        }
        return tipo;
    }
}

const per = new Cliente2('pedro', 20000);
const per2 = new Cliente2('Jose', 502);

console.log(per.tipoCliente())
console.log(per2.tipoCliente());
