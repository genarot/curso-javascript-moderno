//Intermediario que se comunica con distintos objetos
const Vendedor = function( nombre) {
    this.nombre = nombre;
    this.sala   = null;
}

const Comprador = function(nombre) {
    this.nombre = nombre;
    this.sala   = null;
}
Vendedor.prototype = {
    oferta: function(articulo, precio ) {
        console.log(`Tenemos el siguente articulo ${articulo}, en oferta`);
        
    },
    vendido: function( comprador) {
        console.log(`Vendido a ${comprador} `);
    } 
}

Comprador.prototype = {
    oferta: function( mensaje, comprador ){
        console.log(`${comprador.nombre} : ${mensaje} `);
    }
}

const Subasta = function(nombre) {
    let compradores = {};
    this.nombre = nombre;
    return {
        registrar: function(usuario) {
            compradores[usuario.nombre] = usuario;
            usuario.sala = this;
            console.log(compradores)
        }
    }
}

// Instanciar los objectos
const juan = new Comprador('Juan');
const genaro = new Comprador('Genaro');
const jose  = new Comprador('Jose');

const vendedor = new Vendedor('Vendedor 1');
const vend2     = new Vendedor('Vendedor 2');

const subasta   = new Subasta('La subasta');
subasta.registrar(juan)
subasta.registrar(genaro)
subasta.registrar(jose)

console.log(subasta);

vendedor.oferta('Mustang 1966', 3000);
juan.oferta(3500, juan);
genaro.oferta(4000, genaro);
jose.oferta(10000, jose);
vendedor.vendido(jose.nombre)
// console.log(juan);
