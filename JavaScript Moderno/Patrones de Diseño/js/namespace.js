const restaurApp = {};
restaurApp.platillos= [
    {
        platillo: 'Hamburguesa',
        precio: 50
    },
    {
        platillo: 'Hot dog',
        precio: 40
    },
    {
        platillo: 'Sandwich',
        precio: 30
    },
    {
        platillo: 'Pan',
        precio: 2
    }
];


restaurApp.funciones = {
    ordenar: (id) => {
        console.log(`Tu platillo ${restaurApp.platillos[id]} se esta preparando`);
    },
    agregarPlatillo: (platillo, precio) => {
        const nuevo = {
            platillo: platillo,
            precio: precio
        };
        restaurApp.platillos.push( nuevo );
    },
    listarMenu: platillos => {
        console.log(`Bienvenido a nuestro menu`);
        platillos.forEach((platillo, index) => {
            console.log(
                `${index} : ${platillo.platillo}  $${platillo.precio}`
            );   
        });
    }
}

console.log(restaurApp);
const {platillos } = restaurApp;

restaurApp.funciones.ordenar(2);
restaurApp.funciones.listarMenu( platillos)

restaurApp.funciones.agregarPlatillo('Lasaña', 100);

restaurApp.funciones.listarMenu( platillos)