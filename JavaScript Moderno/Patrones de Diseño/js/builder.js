// se crea una abstraccion que permita crear distintos tipos de objetos
class Formulario {
    constructor() {
        this.campos = [];
    }
    
    agregarCampo( tipo, texto ) {
        let campo;
        switch( tipo ) {
            case "text":
                campo   = new InputText(texto);
            break
            case "email":
                campo   = new InputMail(texto);
            break;
            case "button":
                campo   = new Boton(texto);
            break;
            default:
            throw new Error('Tipo no valido: '+ tipo);
        }
        this.campos.push(campo)
    }
    obtenerFormulario() {
        let form = document.createElement('form');
            form.id = 'my-form';
            this.campos.forEach(campo => {
                form.appendChild( campo.crearElemento() );
                form.appendChild( document.createElement('br'));
            })
            return form;
    }
}

class Inputs {
    constructor(texto) {
        this.texto = texto;
    }
}
class InputText extends Inputs{
    constructor(texto ) {
        super(texto);
    }

    crearElemento() {
        const inputText = document.createElement('input');
        inputText.setAttribute('type', 'text');
        inputText.setAttribute('placeholder', this.texto);
        return inputText;
    }
}
class InputMail extends Inputs{
    constructor(texto ) {
        super(texto);
    }

    crearElemento() {
        const inputMail     = document.createElement('input');
        inputMail.setAttribute('type','email');
        inputMail.setAttribute('placeholder',this.texto);
        return inputMail;
    }
}
class Boton extends Inputs{
    constructor(texto ) {
        super(texto);
    }

    crearElemento() {
        const boton = document.createElement('input');
        boton.setAttribute('type',  'submit');
        boton.setAttribute('value',this.texto);
        return boton;
    }
}

//Instanciar formulario
const formulario = new Formulario();

formulario.agregarCampo('text', 'Añade tu Nombre')
formulario.agregarCampo('text', 'Añade tu Apellido')
formulario.agregarCampo('email', 'Agrega tu email');
formulario.agregarCampo('button', 'Boton')

console.log(formulario);
console.log(formulario.obtenerFormulario());

//Esperar que el Cargue el DOM
document.addEventListener('DOMContentLoaded', () => {

    //Renderizar el HTML
    document.querySelector('#app').appendChild(formulario.obtenerFormulario());
})