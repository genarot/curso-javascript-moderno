//Una forma de crear variables publicas y privadas en nuestras Apps
const comprarBoleto = (function() {
    //Privado
    let evento = 'Conferencia JS 2019';

    const adquirirBoleto = () => {
        const elemento = document.createElement('p');
        elemento.textContent = `Comprando boleto para ${evento}`;
        document.querySelector('#app').appendChild(elemento);
    }
    //publico
    return {
        mostrarBoleto: function() {
            console.log(evento);
        },
        adquirirBoleto
    }
})();

comprarBoleto.mostrarBoleto();

comprarBoleto.adquirirBoleto();