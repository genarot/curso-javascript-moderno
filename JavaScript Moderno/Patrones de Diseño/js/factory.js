// Patron de creacion de objetos
//Creacion de objetos que son similares pero no se sabe cual es
function ConstructorSitios() {
    this.crearElemento = (texto, tipo) => {
        let html;
        if ( tipo === 'input') {
            html = new InputHTML(texto);
        } else  if ( tipo === 'img') {
            html = new ImagenHTML(texto);
        } else if ( tipo === 'h1') {
            html = new HeadingHTML( texto);
        } else if ( tipo === 'p' ) {
            html = new ParrafoHTML(texto);
        }
        html.tipo = tipo;
        html.mostrar = function() {
            const elemento = document.createElement(this.tipo);
            if ( this.tipo == 'input' ) {
                elemento.setAttribute('placeholder', this.texto);
            } else if ( this.tipo === 'img' ) {
                elemento.src = texto;
            } else {
                elemento.appendChild(document.createTextNode(this.texto));
            }
            document.querySelector('#app').appendChild(elemento);
        }
        return html;
    }
}
const InputHTML = function(texto) {
    this.texto = texto;
}
const ImagenHTML = function(texto) {
    this.texto = texto;
}
const HeadingHTML = function(texto) {
    this.texto = texto;
}
const ParrafoHTML = function(texto) {
    this.texto = texto;
}

const sitioWeb =new  ConstructorSitios();

//almacenar los elementos
const elementosWeb = [];

elementosWeb.push( sitioWeb.crearElemento('Bienvenidos', 'h1') );
elementosWeb.push( sitioWeb.crearElemento('Bienvenidos a mi sitio web', 'p') );
elementosWeb.push( sitioWeb.crearElemento('https://cms-assets.tutsplus.com/uploads/users/1795/posts/29044/preview_image/AngularvsReact-Preview.jpg', 'img') );
elementosWeb.push( sitioWeb.crearElemento('Conoce mas sobre nosostros', 'h1') );
elementosWeb.push( sitioWeb.crearElemento('Contacto', 'input') );
elementosWeb.push( sitioWeb.crearElemento('Contactanos', 'h1') );

console.log(elementosWeb);
elementosWeb.forEach( elemento => {
    elemento.mostrar();
})