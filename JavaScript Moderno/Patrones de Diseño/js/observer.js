//PAtron de comportamiento
//Tambien se le conoce como subscriptor-publicador

let observer  = {
    obtenerOfertas: function(callback) {
        if( typeof callback === 'function'){ 
            this.subscribers[this.subscribers.length] = callback;
        }
    }, 
    cancelarOfertas: function(callback) {
        for (let i = 0; i < this.subscribers.length; i++) {
            if (this.subscribers[i] === callback) {
                delete this.subscribers[i];
            }
        }
    }, 
    publicarOfertas: function(oferta) {
        for (let i = 0; i < this.subscribers.length; i++) {
            if ( typeof this.subscribers[i] === 'function') {
                this.subscribers[i](oferta);
            }
        }
    },
    crear: function(objeto) {
        for ( let i in this ) {
            if ( this.hasOwnProperty(i)) {
                objeto[i] = this[i];
                objeto.subscribers = [];
            }
        }
     
    }
}

// Publicadores
//Vendedores
const udemy = {
    nuevoCurso: function() {
        const curso = 'Un Nuevo curso de Javascript';
        this.publicarOfertas(curso);
    }
}

const facebook = {
    nuevoAnuncio: function(){
        const oferta ='Compra un Iphone';
        this.publicarOfertas(oferta);
    }
}

//Crear los publicadores
observer.crear(udemy);
observer.crear(facebook);

const juan = {
    compartir: function(oferta) {
        console.log(`Juan dice: Excelente Oferta! ${oferta}`);
    }
};
const karen = {
    interesa: function(oferta) {
        console.log(`Karen dice: Me interesa la oferta de ${oferta}`);
    },
    comprar: function(oferta ) {
        console.log(`Karen dice: Te acepto la oferta de ${oferta}`);
    }
}

console.log(udemy);
console.log(facebook);

udemy.obtenerOfertas( juan.compartir )
udemy.obtenerOfertas( karen.interesa )
udemy.nuevoCurso();
udemy.cancelarOfertas( karen.interesa )
udemy.publicarOfertas('Prras en celo')

facebook.obtenerOfertas( karen.comprar )
facebook.nuevoAnuncio();
facebook.cancelarOfertas( karen.comprar)
facebook.nuevoAnuncio();