const alumnos = {
    //todos los alumnos
    listaAlumnos: [],

    //Obtener un alumno
    get: function(id) {
        console.log(id);
        return this.listaAlumnos[id];
    },

    //crear un alumno
    crear: function( datos ) {
        datos = Object.assign({},{
                nombre: 'Genaro',
                edad: 22
        }, datos)
        this.listaAlumnos.push(datos);
    },

    listado: function() {
        return this.listaAlumnos;
    }

}

const infoAlumno = {
    nombre: 'Jose'
};

const infoAlumno2 = {
    nombre: 'Juan',
    edad: 20
}

alumnos.crear(infoAlumno)
alumnos.crear(infoAlumno2)

console.log(alumnos.listado());
console.log(alumnos.get(0));
