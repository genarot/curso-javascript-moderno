// Traversing Hijo al padre
const enlaces = document.querySelectorAll('.enlace');

console.log(enlaces)
console.log(enlaces[0])
console.log(enlaces[0].parentNode)

const cursos = document.querySelectorAll('.card');

console.log(cursos[0].parentElement)
console.log(cursos[0].parentNode)
console.log(cursos[0].parentElement.parentElement.parentElement.children[0].textContent = 'Hola desde traversing')

//Sibling son los que estan al mismo nivel del elemento seleccionado
console.log(enlaces[4].previousElementSibling.previousElementSibling)

console.log(enlaces[0].nextElementSibling.nextElementSibling.parentElement.children)