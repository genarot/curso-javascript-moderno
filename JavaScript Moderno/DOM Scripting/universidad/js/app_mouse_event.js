//Variables
const encabezado = document.querySelector('#encabezado');
const enlaces   = document.querySelectorAll('.enlace');
const boton = document.querySelector('#vaciar-carrito');

//Click 
// boton.addEventListener('click', obtenerEvento);
// Doble CLick
// boton.addEventListener('dblclick', obtenerEvento);
// Mouse enter
// boton.addEventListener('mouseenter', obtenerEvento );
// Mouse leave (sales del boton)
// boton.addEventListener('mouseleave', obtenerEvento );
// Mouse Over
// boton.addEventListener('mouseover', obtenerEvento );
// Mouse out
// boton.addEventListener('mouseout', obtenerEvento)
// Mouse Down
// boton.addEventListener('mousedown', obtenerEvento);
// Mouse Up
// boton.addEventListener('mouseup', obtenerEvento)

encabezado.addEventListener('mousemove', obtenerEvento)

function obtenerEvento(e) {
    // console.log(e.target);
    console.log(`Evento: ${e.type} en Boton: ${e.target.id}`)
}