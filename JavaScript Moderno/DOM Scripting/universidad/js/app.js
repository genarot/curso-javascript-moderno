// Agregar a Local Storage
localStorage.setItem('nombre', 'Genaro');

sessionStorage.setItem('apellido', 'Tinoco');

//Eliminar del Local storage
// localStorage.removeItem('nombre');

function limpiarLocalStorage(){
    //Limpiar localStorage
    localStorage.clear();
}

//Obtener keys del session y local Storage
console.log(Object.keys(sessionStorage))