//Traversing
const navegacion = document.querySelector('#principal');
//childNodes retorna tambien los espacios

//Retorna los hijos del selector actual
console.log(navegacion.children)

// Nombre de la etiqueta
console.log(navegacion.children[0].nodeName)

// 
console.log(navegacion.children[0].nodeType)
// Tipo
// 1 = Elementos
// 2 = Atributos
// 3 = Text Node
// 8 = comentarios
// 9 = documentos
// 10 - doctype

// Cambiar texto
console.log( navegacion.children[0].textContent = 'Para tu bisne');

const cursos = document.querySelectorAll('.card');

console.log(cursos[0].lastElementChild);
console.log(cursos[0].firstElementChild);
console.log(cursos[0].childElementCount)
