// Event Bubbling
const card = document.querySelector('.card');
const infoCurso = document.querySelector('.info-card');
const agregarCarrito = document.querySelector('.agregar-carrito');

card.addEventListener('click', (e) => {
    console.log('Click en card');
    e.stopPropagation();
});

infoCurso.addEventListener('click', (e) => {
    console.log('Click en Info Curso');
    e.stopPropagation();
});

agregarCarrito.addEventListener('click', (e) => {
    console.log('Click en Agregar Carrito');
    e.stopPropagation();
});