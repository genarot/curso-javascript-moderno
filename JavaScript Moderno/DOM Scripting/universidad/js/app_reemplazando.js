// Reemplazar elementos
const nuevoEncabezado = document.createElement('h2');

nuevoEncabezado.id = 'encabezado';
nuevoEncabezado.style.padding = '10px';
nuevoEncabezado.style.color = '#45d96';
// nuevoEncabezado.textContent = 'Cursos Piratas';
nuevoEncabezado.appendChild(document.createTextNode('Cursos Piratas'));

//Elemento anterior sera remplazado
const anterior = document.querySelector('#encabezado');

const elPadre  = document.querySelector('#lista-cursos');
elPadre.replaceChild( nuevoEncabezado, anterior );

console.log(anterior.parentElement)

