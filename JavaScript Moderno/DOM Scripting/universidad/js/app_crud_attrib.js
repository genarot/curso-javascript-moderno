// Eliminar atributos
const primerLi = document.querySelector('.enlace');

let elemento;

//Obtener una clase de CSS
elemento    = primerLi.className;
elemento    = primerLi.classList;
elemento.add("button")
elemento.add("button-primary")
elemento.remove("button");
elemento.remove("button-primary");

//Leer atributos
elemento  =  primerLi.getAttribute('href');

console.log(elemento);

primerLi.setAttribute('href','#encabezado')
//atributos personalizados
primerLi.setAttribute('data-id', 20);
primerLi.hasAttribute('data-id');

elemento= primerLi;
console.log(elemento)
//verifica que exista
console.log(elemento.hasAttribute('data-id'))

//Remueve atributo
elemento.removeAttribute('data-id')