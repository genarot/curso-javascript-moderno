// getElementById
let elemento;

elemento = document.getElementById('hero');
elemento = document.getElementById('busqueda');

console.log(elemento);

let encabezado;
encabezado = document.getElementById('encabezado').innerText;
encabezado = document.getElementById('encabezado').textContent;
encabezado = document.getElementById('encabezado').classList;
encabezado = document.getElementById('encabezado').innerText;
encabezado = document.getElementById('encabezado');
encabezado.style.background = '#333';
encabezado.style.color  = '#fff';
encabezado.style.padding = '20px';

// Cambiar el texto
encabezado.textContent = 'Los mejores cursos';

console.log(encabezado)