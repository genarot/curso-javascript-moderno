// Query selector
// Retorna el primero
const encabezado = document.querySelector('#encabezado');

// Aplicar CSS
encabezado.style.background = '#333';
encabezado.style.color = '#49b6d5'
encabezado.style.padding= '20px';
encabezado.textContent = 'Los peores cursos';

console.log(encabezado);

// Selecciona todos los que cumplan la condicion
let enlaces = document.querySelectorAll('.enlace');

// por etiqueta
const h1 = document.querySelector('h1');

let enlace;
enlace = document.querySelector('#principal');
enlace = document.querySelector('#principal a:first-child');
enlace = document.querySelector('#principal a:last-child');
enlace = document.querySelector('#principal a:nth-child(3)')

//Selectores multiples
enlaces = document.getElementsByClassName('enlace');
// Enlaces Impares
enlaces  = document.querySelectorAll('#principal a:nth-child(odd)');

console.log(enlaces)
enlaces.forEach(function(impar) {
    impar.style.backgroundColor = '#4d4d4d';
    impar.style.color = '#fff';
})

console.log(enlace)