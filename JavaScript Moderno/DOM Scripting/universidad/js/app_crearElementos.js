// Crear enlaces
const enlace = document.createElement('a');

// agregamos una clase
enlace.className = 'enlace';
// anadir id
enlace.id = 'nuevo-id'
// atributo de href
enlace.setAttribute('href','#');
// anadir texto
// enlace.textContent = 'Nuevo Enlace';
enlace.appendChild(document.createTextNode('Nuevo Enlace'));

// agregarlo al html
let secundaria = document.getElementById('secundaria');
secundaria  = document.querySelector('#secundaria');
secundaria.appendChild(enlace);

console.log(enlace)