// Input Event LIstener
//variables
const busqueda = document.getElementById('buscador');
busqueda.style.color = '#000';
// busqueda.addEventListener( 'keydown', obtenerEvento );
// busqueda.addEventListener( 'keyup', obtenerEvento );
// busqueda.addEventListener( 'keypress', obtenerEvento );
// busqueda.addEventListener('focus',obtenerEvento);
// busqueda.addEventListener('blur',obtenerEvento); // sale del input
// busqueda.addEventListener('cut', obtenerEvento);
// busqueda.addEventListener('copy', obtenerEvento);
busqueda.addEventListener('paste', obtenerEvento);
busqueda.addEventListener('input', obtenerEvento); //retorna todos los demas
busqueda.addEventListener('change', obtenerEvento);// para los select


function obtenerEvento(e) {
    console.log(e.target.value);
    document.querySelector('#encabezado').innerText = e.target.value;
    console.log(`Evento: ${e.type}`);
}