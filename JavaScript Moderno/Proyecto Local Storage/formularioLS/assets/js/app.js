const listaTweets = document.getElementById('lista-tweets');

// Event Listeners
eventListeners();

function eventListeners() {
    // CUando se envia el formulario
    document.querySelector('#formulario').addEventListener('submit',
        agregarTweet);

    // Borrar Tweets
    listaTweets.addEventListener('click', borrarTweet)

    // Contenido cargado
    document.addEventListener('DOMContentLoaded', localStorageListo)
}

// Funciones

// Añadir tweet del formulario
function agregarTweet(e) {
    e.preventDefault();
    console.log('Formulario Enviado');
    //leer el valor del textArea
    const tweet = document.getElementById('tweet');
    //Crear el boton eliminar tweet
    const botonBorrar = document.createElement('a');
    botonBorrar.classList.add('borrar-tweet');
    botonBorrar.innerText = 'x'; 
    // botonBorrar.addEventListener('click', eliminarTweet);

    //Crear elemento y anadir a la lista
    const li    = document.createElement('li');
    // li.appendChild( document.createTextNode(tweet.value));
    li.innerText = tweet.value;
    li.appendChild(botonBorrar);
    listaTweets.appendChild( li )

    console.log(tweet.value);
    agregarTweetLocalStorage(tweet.value);
}

//Eliminar tweet del DOM
function borrarTweet(e) {
    e.preventDefault();
    if ( e.target.classList.contains('borrar-tweet') ) {
        if ( confirm('Desea eliminar el tweet?') ) {
            // console.log(e.target.parentElement.innerText)
            borrarTweetLocalStorage(e.target.parentElement.innerText)
            e.target.parentElement.remove();
        }
    } else {
        console.log('Diste click en otra parte');
    }
}

//Agrega el Tweet al localstorage
function agregarTweetLocalStorage( tweet ) {
    let tweets;
    tweets = obtenerTweetsLocalStorage();
    //  Como Arreglo
    tweets.push(tweet);
    localStorage.setItem('tweets', JSON.stringify(tweets) );
}

// Mostrar datos del localStorage en la lista
function localStorageListo() {
    let tweets ;
    tweets = obtenerTweetsLocalStorage();
    tweets.forEach(tweet => {
        const botonBorrar = document.createElement('a');
        botonBorrar.className   = 'borrar-tweet';
        botonBorrar.innerText   = 'x';

        const li        = document.createElement('li');
        li.textContent  = tweet;
        li.appendChild(botonBorrar);

        listaTweets.appendChild(li)
    });
}

//Comprobar que haya elementos en el localstorage
function obtenerTweetsLocalStorage() {
    let tweets;
    //Revisamos los valores del local storage
    if ( localStorage.getItem('tweets') === null ) {
        console.warn("No hay tweets almacenados");
        tweets  = [];
    } else {
        // tweets  = localStorage.getItem('tweets').split(',');
        tweets      = JSON.parse(localStorage.getItem('tweets'));
        // console.log(tweets)
    }
    return tweets;
}

//Eliminar tweet del local storate
function borrarTweetLocalStorage(tweet) {
    let tweets, tweetBorrar;
    console.log(tweet);
    //Elimina la x del tweet
    tweetBorrar = tweet.substring(0,tweet.length- 1);
    
    tweets      = obtenerTweetsLocalStorage();
    tweets.forEach((tweet, index) => {
        if ( tweet === tweetBorrar ) {
            tweets.splice(index, 1);
        }
    });
    localStorage.setItem('tweets', JSON.stringify(tweets))
    console.log(tweetBorrar)
    console.log(tweets)
}