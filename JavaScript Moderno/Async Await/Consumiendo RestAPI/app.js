async function leerTodos() {
    //esperar la respuesta
    const respuesta = await fetch('https://jsonplaceholder.typicode.com/todos');

    //procede cuando la respuesta este hecha
    const datos = respuesta.json();
    
    return datos;
}

console.log(leerTodos());
leerTodos()
.then((todos) => {
    console.log(todos);
    
})
.catch(err => console.log('errpr'))