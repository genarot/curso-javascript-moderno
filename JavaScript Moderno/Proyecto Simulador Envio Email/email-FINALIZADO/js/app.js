// Variables
const email = document.getElementById('email');
const asunto = document.getElementById('asunto');
const mensaje = document.getElementById('mensaje');
const btnEnviar= document.getElementById('enviar');
const formularioEnviar = document.getElementById('enviar-mail');
const resetBtn  = document.getElementById('resetBtn');


// event Listener
function  eventListeners(){
    //Inicio de la aplicacion y deshabilitar boton
    document.addEventListener('DOMContentLoaded', inicioApp);
    
    // Campos del formulario
    email.addEventListener('blur',      validarCampo);
    asunto.addEventListener('blur',     validarCampo);
    mensaje.addEventListener('blur',    validarCampo);

    // Boton de enviar en el submit
    formularioEnviar.addEventListener('submit', enviarEmail)
    // btnEnviar.addEventListener('click', clickBoton)

    //Boton de reset
    resetBtn.addEventListener('click', resetFormulario)
}
eventListeners();

// Funciones

//
function inicioApp() {
    // Deshabilitar el envio
    btnEnviar.disabled = true;
}

//Valida que el campo tenga algo escrito
function validarCampo( evt ) {
    console.log('dentro del INPUT');

    // Se valida la longitud del texto y que no este vacio
    validarLongitud( this );

    //validar unicamente email
    if ( this.type == 'email' ) {
        validarEmail( this );
    }

    let errores = document.querySelectorAll('.errores');
    if ( email.value !== '' && asunto.value !== '' && mensaje.value !== '' ) {
        if ( errores.length === 0) {
            btnEnviar.disabled = false;
        }
    }
}

//Verifica la longitud de texto en los campos
function validarLongitud( campo ) {
    console.log(campo.value.trim());
    if ( campo.value.trim().length > 0 ) {
        // campo.style.borderBottomColor = 'green';
        campo.style.borderBottom = '1px solid green';
        campo.classList.remove('error');
    } else {
        campo.style.borderBottom = '2px solid red';
        campo.classList.add('error');
    }
    campo.value = campo.value.trim();
}

//
function validarEmail( campo) {
    const mensaje = campo.value;
    if ( mensaje.indexOf('@')  !== -1) {
        campo.style.borderBottom = '1px solid green';
        campo.classList.remove('error');
    } else {
        campo.style.borderBottom = '2px solid red';
        campo.classList.add('error');
    }
}

// cuando se envia el correo
function enviarEmail( evt ) {
    //Spinner al presionar enviar
    const spinnerGif    = document.querySelector('#spinner');
    spinnerGif.style.display = "block";
    
    // GIf que envia email
    const enviado = document.createElement('img');
    enviado.src = 'img/mail.gif';
    enviado.style.display = 'block';
    console.log(evt.target.getLastChild)
    // evt.target.appendChild(enviado);
    evt.target.lastElementChild.style.display = 'none';
    //Ocultar espiner y mostrar gif de enviado
    setTimeout(function(){
        spinnerGif.style.display = 'none';
        evt.target.querySelector('#loaders').appendChild(enviado);
        setTimeout(function(){
            enviado.remove();
            formularioEnviar.lastElementChild.style.display = 'block';
            formularioEnviar.reset();
        }, 3000)
    }, 3000);
    return false;
}

//Resetear el formulario
function resetFormulario( evt ) {
    formularioEnviar.reset();
    e.preventDefault();
}