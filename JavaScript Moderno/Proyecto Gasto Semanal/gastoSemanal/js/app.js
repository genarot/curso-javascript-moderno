// Variables
const presupuestoUsuario = new Number(prompt('Cual es tu presupuesto semanal?'));
let     cantidadPresupuesto;
let     formulario      = document.getElementById('agregar-gasto');

//Clases
class Presupuesto {
    constructor( presupuesto ) {
        this.presupuesto = presupuesto;
        this.restante = presupuesto;
    }

    //Metodo para ir restando del presupuesot actual
    agregarGasto ( cantidad = 0 ) {
        console.log(this.restante);
        this.restante -= Number(cantidad);
        console.log(this.restante);
    }
}
//Maneja todo lo relacionado a el HTML
class Interfaz  {
    insertarPresupuesto ( cantidad ) {
        const presupuestoSpan = document.querySelector('span#total');
        const restanteSpan    = document.querySelector('span#restante');

        presupuestoSpan.innerHTML   =  cantidad;
        restanteSpan.innerHTML      = cantidad;
    }

    insertarPresupuestoRestante( restante ) {
        const restanteSpan      =   document.querySelector('span#restante');
        restanteSpan.innerHTML  =  restante;
        this.comprobarPresupuesto();
    }
    
    comprobarPresupuesto() {
        const presupuestoTotal = cantidadPresupuesto.presupuesto;
        const presupuestoRestante = cantidadPresupuesto.restante;

        // COmprobar el 25%
        if ( (presupuestoTotal / 4 ) > presupuestoRestante ) {
            const restante = document.querySelector('.restante');
            restante.classList.remove('alert-success', 'alert-warning');
            restante.classList.add('alert-danger')
        } else if (  (presupuestoTotal / 2) > presupuestoRestante ) {
            const restante = document.querySelector('.restante');
            restante.classList.remove('alert-success');
            restante.classList.add('alert-warning')
        }
    }

    imprimirMensaje( mensaje, tipo ) {
        const divMensaje = document.createElement('div');
        divMensaje.classList.add('text-center', 'alert');
        if ( tipo === 'error' ) {
            divMensaje.classList.add('alert-danger');
        } else {
            divMensaje.classList.add('alert-success');
        }
        divMensaje.appendChild( document.createTextNode(mensaje) );
        //Insertar en el DOM
        document.querySelector('.primario').insertBefore(divMensaje, formulario);

        //Quitar el alert despues de 3 segundos
        setTimeout(function(){
            divMensaje.remove();
            formulario.reset();
        }, 3000)
    }

    //Insertar gasto a la lista
    insertartGastoListado( nombre, cantidad ) {
        const gastosListado = document.querySelector('#gastos ul');

        // Crear un Li
        const li = document.createElement('li');
        li.className = 'list-group-item d-flex justify-content-between all200ign-items-center';
        //Insertar el gasto
        li.innerHTML = `
            ${nombre}
            <span class="badge badge-primary badge-pill"> $ ${cantidad} </span>
        `;

        //Insertar al DOM
        gastosListado.appendChild(li)
        formulario.reset();
    }
}

//Event Listeners
document.addEventListener('DOMContentLoaded', function() {
    if ( presupuestoUsuario === null || presupuestoUsuario == 0 || isNaN(presupuestoUsuario) ){
        window.location.reload();
    } else {
        //Instanciar un presupuesto
        cantidadPresupuesto = new Presupuesto( presupuestoUsuario );
        console.log(cantidadPresupuesto);
        //Instanciaa la clase de intefaz
        const ui = new Interfaz();
        ui.insertarPresupuesto( cantidadPresupuesto.presupuesto );
        ui.insertarPresupuestoRestante( cantidadPresupuesto.restante );
    }
});

formulario.addEventListener('submit', function(e) {
    e.preventDefault();
    // Leer el formulario de gastos
    const nombreGasto = formulario.querySelector('#gasto').value;
    const cantidadGasto = formulario.querySelector('#cantidad').value;

    cantidadPresupuesto.agregarGasto( cantidadGasto );
    const ui = new Interfaz(); 
    if ( nombreGasto === '' || cantidadGasto === '' ) {
        console.warn('Hubo un error');
        /*  
            mensaje
            tipo
        */
        ui.imprimirMensaje('Hubo un error', 'error')        
    } else {
        ui.insertarPresupuestoRestante(cantidadPresupuesto.restante)
        ui.insertartGastoListado( nombreGasto, cantidadGasto )
    }
    
    // console.log(nombreGasto);
})

// console.log(presupuestoUsuario);
