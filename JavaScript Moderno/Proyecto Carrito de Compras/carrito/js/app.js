//Variables
let carrito = document.querySelector('#carrito');
let listaCursos = document.querySelector('#lista-cursos');
const listaCarrito = document.querySelector('#lista-carrito tbody');
const vaciarCarritoBtn = document.getElementById('vaciar-carrito');

//Listener
cargarEventListeners();

function cargarEventListeners() {
    //Dispara cuando se presiona Agregar a Carrito
    listaCursos.addEventListener('click', comprarCurso);

    // Dispara cuando se preciosa Borrar Curso
    carrito.addEventListener('click', eliminarCurso)

    // A vaciar el carrito
    vaciarCarritoBtn.addEventListener('click', vaciarCarrito)

    document.addEventListener('DOMContentLoaded', cursosCarritoListar)
}

//Funciones
function comprarCurso(evt) {
    evt.preventDefault();
    //Funcion que anade el curso al carrito
    if (evt.target.classList.contains('agregar-carrito')) {
        console.log('Si')
        let curso   = evt.target.parentElement.parentElement;
        // console.log(evt.target.parentElement)
        leerDatosCurso(curso);
    }
}

//Lee los datos del curso
function leerDatosCurso( curso ) {
    const infoCurso = {
        // imagen: curso.querySelector('.imagen-curso').getAttribute('src'),
        imagen: curso.querySelector('img').src,
        titulo: curso.querySelector('h4').textContent,
        precio: curso.querySelector('.precio span').textContent,
        id:     curso.querySelector('a').getAttribute('data-id')
    }
    console.log(infoCurso)
    insertarCarrito(infoCurso)
    // console.log(curso);
    // console.log(curso.children);
    // console.log(curso.children[1])
}

// Muestra el curso seleccionado en el carrito
function insertarCarrito(curso) {
    const row   = document.createElement('tr');
    row.innerHTML = `
        <td>
            <img src="${curso.imagen}" width="100px"/>
        </td>
        <td>
            ${curso.titulo}
        </td>
        <td>
            ${curso.precio}
        </td>
        <td>
            <a href="#" class="borrar-curso" data-id="${curso.id}" >
                x
            </a>
        </td>
    `;
    console.log(listaCarrito)
    listaCarrito.appendChild(row);
    agregarCursoLocalStorage(curso);
    // const imagen = document.createElement('img');
}

// Elimina el curso del carrito del DOM
function eliminarCurso( evt ) {
    evt.preventDefault();
    let curso;
    if ( evt.target.classList.contains('borrar-curso') ) {
        console.log('eliminado')
        let curso =  evt.target.parentElement.parentElement;
        let cursoId = curso.querySelector('a').getAttribute('data-id');
        eliminarCursoLocalStorage(cursoId);
        evt.target.parentElement.parentElement.remove();
    }
}

// Elimina los cursos del carrito en el DOM
function vaciarCarrito(evt) {
    // Forma lenta
    // listaCarrito.innerHTML = '';
    // Forma rapida (recomentada)
    while( listaCarrito.firstChild ) {
        listaCarrito.removeChild(listaCarrito.firstChild)
    }
    
    //vaciar Local Storage
    vaciarLocalStorage()

    return false;
}

// Obtiene los cursos 
function obtenerCursosLocalStorage() {
    let cursos;
    if ( localStorage.getItem('cursos') === null) {
        cursos = [];
    } else {
        cursos = JSON.parse(localStorage.getItem('cursos'));
    }
    return cursos;
}

// Agregar curso al localStorage
function agregarCursoLocalStorage( curso ) {
    let cursos = obtenerCursosLocalStorage();

    cursos.push(curso);
    localStorage.setItem('cursos', JSON.stringify(cursos));
}

//
function cursosCarritoListar() {
    let cursos = obtenerCursosLocalStorage();
    cursos.forEach(curso => {
        const row   = document.createElement('tr');
        row.innerHTML = `
            <td>
                <img src="${curso.imagen}" width="100px"/>
            </td>
            <td>
                ${curso.titulo}
            </td>
            <td>
                ${curso.precio}
            </td>
            <td>
                <a href="#" class="borrar-curso" data-id="${curso.id}" >
                    x
                </a>
            </td>
        `;
        listaCarrito.appendChild(row);
    });
}

function eliminarCursoLocalStorage( cursoId ) {
    const cursosLS = obtenerCursosLocalStorage();
    
    //Iteramos comparando el Id del curso borrado con los del LS
    cursosLS.forEach((cursoLs, index) => {
        if ( cursoLs.id ==  cursoId ) {
            cursosLS.splice(index, 1);
        }
    });
    localStorage.setItem('cursos', JSON.stringify(cursosLS));
    // console.log(curso)
} 

//Elimina todos los cursos del local storage
function vaciarLocalStorage() {
    localStorage.clear();
}