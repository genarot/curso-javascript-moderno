//Variables
const eventBrite    = new EventBrite();
const ui            = new Interfaz();


// Event Listeners
//Listener 
document.getElementById('buscarBtn').addEventListener('click', (e) => {
    e.preventDefault()

    //Leer el texto del input buscar
    const textoBuscador = document.getElementById('evento').value;

    // Leer el select categoria
    const categorias    = document.getElementById('listado-categorias');
    const categoriaSeleccionada = categorias.options[categorias.options.selectedIndex].value;

    //Revisar que haya algo escrito en el buscador
    if ( textoBuscador.trim() !== '' ) {
        console.log('Buscando...');
        eventBrite.obtenerEventos( textoBuscador.trim(), categoriaSeleccionada )
        .then( data => {
            if ( data.events.length > 0 ) {
                //Si hay eventos, mostrar el resultado
                ui.limpiarResultados();
                ui.mostrarEventos( data )
            } else {
                //No hay eventos, mostrar una alerta
                ui.mostrarMensaje('No hay resultados', 'alert alert-danger mt-4')
            }
            console.log(data);
            
        })
        .catch(err => console.error(err) )
    } else {
        //Mostrar mensaje para que imprima algo
        ui.mostrarMensaje('Escribe algo en el buscador', 'alert alert-danger mt-4')
        console.warn('no hay nada');
    }
    
});
