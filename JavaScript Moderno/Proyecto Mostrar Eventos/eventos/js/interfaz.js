class Interfaz {
    constructor() {
        //Inicializa la app al instanciar
        this.init();
        // Leer ek resultado
        this.listado = document.getElementById('resultado-eventos');
    }
    
    init() {
        this.imprimirCategorias();
    }

    //Imprimir categorias de Eventos
    imprimirCategorias(){
        const listaCategorias = 
        eventBrite.obtenerCategorias()
        .then(categorias => {
            const cats = categorias.categories;
            // console.log(categorias);
            // console.log(cats);
            //Seleccionar el select de categorias
            const selectCategoria = document.getElementById('listado-categorias');

            //Recorremos el arreglo e imprimimos los <options>
            cats.forEach(cat => {
                const option = document.createElement('option');
                option.value = cat.id;
                option.appendChild( document.createTextNode(cat.name_localized) );
                selectCategoria.appendChild(option);
            });
        })
        .catch( err => console.error(err))
    }

    //Metodo para imrpimir mensajes: 2 param mensaje y clases
    mostrarMensaje( mensaje, clases ) {
        const div = document.createElement('div');
        div.className = clases;
        //Agregar el texto
        div.appendChild( document.createTextNode(mensaje));
        //Buscar padre
        const buscadorDiv =  document.getElementById('buscador');
        // buscadorDiv.insertBefore( div, buscadorDiv.children[1])
        buscadorDiv.appendChild(div);
        setTimeout(() => {
            this.limpiarMensaje();
        }, 2500);
    }

    //Desaparece mensaje en caso de que exista
    limpiarMensaje() {
        const alert = document.querySelector('.alert');
        if ( alert ) 
            alert.remove();
    }

    //Lee la respuesta del API e imprime los resultados
    mostrarEventos( eventos ) {
        //Leer los eventos y agregarlos a una variable
        const listaEventos = eventos.events;

        //Recorrer los eventos y crear su template
        listaEventos.forEach( evento => {
            this.listado.innerHTML += `
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <img class="img-fluid mb-2" src="${ evento.logo ? evento.logo.url : ''}"/> 
                        </div>
                        <div class="card-body">
                            <div class="card-text">
                                <h2 class="text-center">${evento.name.text}</h2>
                                <p class="lead text-info">Informacion del evento</p>
                                <p>${evento.description.text.substring(0,280)}...</p>

                                <span class="badge badge-primary">Capacidad: ${evento.capacity }</span>
                                <span class="badge badge-secondary">Fecha y Hora: ${evento.start.local }</span>
                                <a href="${evento.url}" target="_blank" class="btn btn-primary btn-block mt-4">Comprar Boletos</a>
                            </div>
                        </div>
                    </div>
                </div>
            `
        })
        console.log(listaEventos);
        
    }

    //Limpia los resultados previos
    limpiarResultados() {

        while ( this.listado.firstChild ) {
            this.listado.removeChild(this.listado.firstChild);
        }
    }
}