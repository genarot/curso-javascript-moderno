class EventBrite {

    constructor() {
        this.token_auth = '37IC2UCENTI4KE3DPJ3W';
        this.ordernar =  'date';
    }

    //Obtenie las categorias en init();
    async obtenerCategorias() {
        //Consultar las categorias a la REST API de event Brite
        const  respuestaCategoria = await fetch(`https://www.eventbriteapi.com/v3/categories/?token=${this.token_auth}`)

        //Esperar las respuestas de las categorias y enviar un JSON
        const categorias   = await respuestaCategoria.json();
    
        //devolvemos el resultado 
        return categorias
    }

    //Mostrar Resultados de la busqueda
    async obtenerEventos( evento, categoriaId ) {
        //Consultar los eventos que cumplan con los parametros enviados
        const respuestaEventos = await fetch(`https://www.eventbriteapi.com/v3/events/search/?q=${evento}&sort_by=${this.ordernar}&categories=${categoriaId}&token=${this.token_auth}`)
        
        //Esperar la respuesta de los eventos y enviarlo como JSON
        const eventos = await respuestaEventos.json();

        return eventos;
    }
}