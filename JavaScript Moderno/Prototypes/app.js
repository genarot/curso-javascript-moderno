// Object Create
const Cliente = {
    imprimirSaldo: function () {
        return `Hola ${this.nombre}, tu saldo es ${this.saldo}`
    }, 
    retirarSaldo: function(retiro){
        return this.saldo -= retiro;
    }
};

// Crear el objeto
const cliente1 = Object.create(Cliente);
cliente1.nombre = 'Genaro';
cliente1.saldo = 1000;

console.log(cliente1);
cliente1.retirarSaldo(300);
console.log(cliente1.imprimirSaldo());