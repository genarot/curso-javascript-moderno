function Cliente( nombre, saldo ) {
    this.nombre = nombre;
    this.saldo = saldo;
}
//Crear un Prototype
Cliente.prototype.tipoCliente = function() {
    let tipo;
    if ( this.saldo > 1000 ) {
        tipo = 'Gold';
    }  else if ( this.saldo > 500 ) {
        tipo = 'Platinum';
    }
    else {
        tipo = 'Normal'
    }
    return tipo;
};
// Prototipo que imprime el nombre y el saldo
Cliente.prototype.nombreClienteSaldo = function() {
    return `Nombre: ${this.nombre}, Tu saldo es de ${this.saldo}.
    Tipo Cliente ${this.tipoCliente()}`;
}

Cliente.prototype.retirarSaldo = function(retiro) {
    return this.saldo -= retiro;
}

// Banca para empresas
function Empresa(nombre, saldo, telefono, tipo ) {
    // Herencia
    Cliente.call(this, nombre, saldo);
    this.telefono = telefono;
    this.tipo = tipo;
}
//Heredar Prototype
Empresa.prototype   = Object.create(Cliente.prototype);

const cliente1 = new Cliente('Genaro', '2000');
const empresa1 = new Empresa('AtomicDev', 1230000, '23132362', 'desarrollo');

console.log(cliente1)
console.log(cliente1.nombreClienteSaldo());

console.log(empresa1)
console.log(empresa1.nombreClienteSaldo());