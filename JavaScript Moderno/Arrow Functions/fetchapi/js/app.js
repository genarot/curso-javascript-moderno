// Event Listeners
document.getElementById('txtBtn').addEventListener('click', cargarTXT);
document.getElementById('jsonBtn').addEventListener('click', cargarJSON);
document.getElementById('apiBTN').addEventListener('click', cargarREST)

// Cargando un archivo local
function cargarTXT() {
    fetch('datos.txt')
        .then( res => {
            if ( res.status === 200 ) {
                return res.text();
            } else {
                console.log('Ocurrio un error');
                console.warn(`Url: ${res.url} , Error: ${res.statusText}`);
                return;
            }
        })
        .then( respuesta  => {
            if ( respuesta !== undefined) {
                document.getElementById('resultado').innerHTML = respuesta;

            }
        })
        .catch( err => console.error(err) )
}

// Consumiendo un JSON local
function cargarJSON() {
    fetch('empleados.json')
    .then((res) => {
        console.log(res);
        return res.json()
    })
    .then((empleados) => {
        console.log(empleados);
        let html = '';
        empleados.forEach(empleado => {
            html +=   `
                <li>${empleado.nombre} ${empleado.puesto} </li>
            `
        });
        document.getElementById('resultado').innerHTML = html;
    })
    .catch( err => console.error(err) )
}

// Consumiendo API
function cargarREST() {
    fetch('https://picsum.photos/list')
    .then( res =>  res.json() )
    .then( imagenes => {
        console.log(imagenes);
        
        let html = '';
        imagenes.forEach((imagen) => {
          html += `<li>
                        <a href="${imagen.post_url}" target="_blank">Ver Imagen</a>
                        ${imagen.author}
                    </li>
          `;
        })
        document.getElementById('resultado').innerHTML = html;
    })
    .catch( err => console.error(err) )
}