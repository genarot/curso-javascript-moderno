let aprendiendo;

aprendiendo = function() {
    console.log('Aprendiendo JavaScript');
}

aprendiendo = () => {
    console.log('Aprendiendo JavaScript');
}

// una linea no requuiere llave
aprendiendo = () => console.log('Aprendiendo JavaScript');
//REtorna valor
aprendiendo = () => 'Apreniendo JavaScript'
//Retorna objeto
aprendiendo = () => ({aprendiendo: 'Aprendiendo JavaScript'})
//Pasar parametros
aprendiendo = (tecnologia) => console.log(`Aprendiendo ${tecnologia}`);
// Pasar un solo parametro
aprendiendo = tecnologia => console.log(`Aprendiendo ${tecnologia}`);

console.log( aprendiendo('ES6') );

const productos = ['Disco', 'Uno', 'Dolar'];

let  letrasProducto;
letrasProducto = productos.map((producto) => {
    return producto.length;
})
letrasProducto = productos.map(producto => producto.length)

console.log(letrasProducto);
