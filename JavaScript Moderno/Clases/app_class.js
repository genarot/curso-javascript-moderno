class Cliente {
    constructor(nombre, apellido, saldo) {
        this.nombre     = nombre;
        this.apellido   = apellido;
        this.saldo      = saldo;
    }

    imprimirSaldo() {
        return `Hola ${this.nombre}, tu saldo es de ${this.saldo}`;
    }

    tipoCliente() { 
        let tipo;
        if ( this.saldo  > 10000) {
            tipo = 'Gold';
        } else if (this.saldo > 5000 ) {
            tipo = 'Platinum';
        } else {
            tipo = 'Normal';
        }
        return tipo;
    }

    retirarSaldo(retiro) {
        return this.saldo -= retiro;
    }

    // solo sirven con la clase, no requieren nueva instancia
    static bienvenida(){
        return `Bienvenido al cajero`;
    }
}

console.log(Cliente.bienvenida());

const genaro = new Cliente('Genaro', 'Tinoco', 99999);

console.log(genaro);
console.log(genaro.imprimirSaldo());
console.log(genaro.retirarSaldo(400));