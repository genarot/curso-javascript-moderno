//Clases y Herencia ECMAScript 6
class Cliente {
    constructor(nombre, saldo) {
        this.nombre = nombre;
        this.saldo  = saldo;
    }

    imprimirSaldo(){
        return  `Hola ${this.nombre}, tu saldo es de: ${this.saldo}`
    }

    static bienvenida() {
        return 'Bienvenido al cajero';
    }
}

class Empresa extends Cliente {
    constructor( nombre, saldo, telefono, tipo) {
        // Referencia al construcor padre
        super(nombre, saldo);
        // No existen en el constructor padre
        this.telefono   = telefono;
        this.tipo       = tipo;
    }

    static bienvenida() {
        return `Bienvenido al cajero para Empresas`;
    }
}

const empresa1 = new Empresa('AtomicDev', 99999, '23132362', 'Desarrollo');
const cliente = new Cliente('NicaLac', 1234);

console.log(Empresa.bienvenida());
console.log(Cliente.bienvenida());


console.log(empresa1);
console.log(empresa1.imprimirSaldo());

console.log(cliente);