import * as UI  from './interfaz.js';
import {API}    from './api.js';

UI.formularioBuscar.addEventListener('submit', evt => {
    evt.preventDefault();

    //Obtener los datos del formulario
    const artista = UI.formularioBuscar.querySelector('#artista').value,
            cancion = UI.formularioBuscar.querySelector('#cancion').value;
    if ( artista.trim() === '' || cancion.trim() === '' ){ 
        UI.divMensajes.innerHTML = 'Error.. Todos los campos son  obligatorios';
        UI.divMensajes.classList.add('error');

        //Limpiar mensaje
        setTimeout(() => {
            UI.divMensajes.innerHTML = '';
            UI.divMensajes.classList.remove('error');
        }, 2500)
    } else {
        //El formulario esta completo
        const api = new API(artista, cancion);
        api.consultarAPI()
        .then( data => {
            let { lyrics: letra} = data;
            // console.log(letra);
            if ( letra ) {
                console.log('Si existe');
                UI.divResultado.textContent = letra;
            } else {
                //La cancion no existe
                console.warn('No existe');
                UI.divMensajes.innerHTML = 'La cancion buscada no existe, prueba con otra busqueda';
                UI.divMensajes.classList.add('error');
                setTimeout(() => {
                    UI.divMensajes.innerHTML = '';
                    UI.divMensajes.classList.remove('error');
                    UI.formularioBuscar.reset();
                }, 2500)
            }
        })
        .catch( err => console.error( err ) )
    }
    console.log(`Artista: ${artista}, Cancion ${cancion}`);
    
})

UI.formularioBuscar.addEventListener('reset', evt => {
    UI.divResultado.innerHTML ='';
})