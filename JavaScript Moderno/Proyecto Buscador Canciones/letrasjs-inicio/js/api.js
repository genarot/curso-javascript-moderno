export class API {

    constructor( artista, cancion) {
        this.artista = artista;
        this.cancion = cancion;
    }

    async consultarAPI() {
        const urlCanciones = `https://api.lyrics.ovh/v1/${this.artista}/${this.cancion}`;
        
        const fetchSongs = await fetch(urlCanciones);

        const datos = await fetchSongs.json();

        return datos;
    }
}